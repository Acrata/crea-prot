<?php
/**
 * Customizer sections.
 *
 * @package Crea 2
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function creaser_hec_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'creaser_hec_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'creaser' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'creaser_hec_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'creaser' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'creaser_hec_customize_sections' );
