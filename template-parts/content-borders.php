<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Crea 2
 */

?>
<div id="borders">
    <div class="left"></div>
    <div class="top">
      <div class="logo">
        <?php get_template_part('template-parts/inline','logotipo.svg');?>
      </div>
    </div>
    <div class="right"></div>
    <div class="bottom"></div>
</div>

<section id="trigger-container">
  <a href="#" class="menu-trigger" id="menu03">
    <span></span>
    <span></span>
    <span></span>
  </a>
</section>