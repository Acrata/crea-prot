<?php
/**
 *  The template used for displaying fifty/fifty text/media.
 *
 * @package Crea 2
 */

// Set up fields.
$image_data = get_sub_field( 'media_right' );
$text = get_sub_field( 'text_primary' );
$animation_class = creaser_hec_get_animation_class();

// Start a <container> with a possible media background.
creaser_hec_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block container fifty-fifty fifty-text-media', // Container class.
) );
?>
	<div class="row <?php echo esc_attr( $animation_class ) ?>">

		<div class="col col-l-6">
			<?php echo force_balance_tags( $text ); // WPCS: XSS OK. ?>
		</div>

		<div class="col col-l-6 ">
			<img class="fifty-media-image" src="<?php echo esc_url( $image_data['url'] ); ?>" alt="<?php echo esc_html( $image_data['alt'] ); ?>">
		</div>

	</div><!-- .row -->
</section><!-- .fifty-text-media -->
