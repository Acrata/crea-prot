<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Crea 2
 */

?>

<section class="section-scaffolding">

	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'creaser' ); ?></h2>


</section>
