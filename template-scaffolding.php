<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, creaser_hec_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Crea 2
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'creaser_hec_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
