<?php
/**
 * Template Name: Home
 *
 * Template Post Type: page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Crea 2
 */

get_header(); ?>

<section class="hero-intro">
<?php get_template_part('template-parts/inline','logotipohero.svg');?>
</section>
<?php $terms = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false,
) ); 
// print_r($terms);
?>

<section class="hero-quote">
<h2>Headline!</h2>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem aspernatur ex iusto, illo totam officiis recusandae sequi ducimus similique, natus omnis harum accusamus numquam? Dolorum neque doloribus amet mollitia ducimus deserunt maiores tempora reiciendis?</p>
</section>
<?php get_footer(); ?>
