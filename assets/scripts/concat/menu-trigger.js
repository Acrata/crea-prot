
// (function(){

Barba.Pjax.start();
window.menuTrigger = {};
  var menuPositionTop;
  var menuPositionLeft;
  var navWidth;
( function( window, $, TweenMax ,app ) {
$(function(){
  var menuComplete = () => {
    console.log("menu complete");
     menuPositionTop = $("#primary-menu").offset().top;
     menuPositionLeft = $("#primary-menu").offset().left;
     navWidth = $('#site-navigation').width();
     console.log("nav width is : " + navWidth);
       console.log(menuPositionTop + " . " + menuPositionLeft);
      // return menuPositionLeft;
  };
  menuComplete();

      TweenMax.set("svg#logotipo-svg-logo",{scale:1});

      // console.log(menuPositionTop + " . " + menuPositionLeft);
      var Anim = TweenMax.to( "#site-navigation" , .4  ,{ x:"0%" , reversed:true ,paused:true, ease: Power2.easeInOut, onComplete: menuComplete }) ;
      var AnimAfter = TweenMax.to( "#site-navigation::after" , .4  ,{ x:"0%" , reversed:true ,paused:true, ease: Power2.easeInOut },3) ;
      var AnimBorder = TweenMax.to( "#borders .top, #borders .bottom" , .4  ,{ height:"70px" , reversed:true ,paused:true, ease: Power2.easeInOut }) ;
      var AnimLogo = TweenMax.to( "svg#logotipo-svg-logo" , .5  ,{ x:-menuPositionLeft / 3,y:menuPositionTop / 2,scale:2 , reversed:true ,paused:true, ease: Power2.easeInOut }) ;

    $('.menu-trigger').on('click', function() {
      $(this).toggleClass('active');
      Anim.reversed() ? Anim.play() : Anim.reverse();
      AnimAfter.reversed() ? AnimAfter.play() : AnimAfter.reverse();
      AnimBorder.reversed() ? AnimBorder.play() : AnimBorder.reverse();
      AnimLogo.reversed() ? AnimLogo.play() : AnimLogo.reverse();
      return false;
    });
  });
    console.log("trigger");
     }(window, jQuery, TweenMax, window.menuTrigger));
