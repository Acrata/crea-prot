'use strict';

/**
 * File hero-carousel.js
 *
 * Create a carousel if we have more than one hero slide.
 */
window.wdsHeroCarousel = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			heroCarousel: $('.carousel')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.doSlick);
		app.$c.window.on('load', app.doFirstAnimation);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.heroCarousel.length;
	};

	// Animate the first slide on window load.
	app.doFirstAnimation = function () {

		// Get the first slide content area and animation attribute.
		var firstSlide = app.$c.heroCarousel.find('[data-slick-index=0]'),
		    firstSlideContent = firstSlide.find('.hero-content'),
		    firstAnimation = firstSlideContent.attr('data-animation');

		// Add the animation class to the first slide.
		firstSlideContent.addClass(firstAnimation);
	};

	// Animate the slide content.
	app.doAnimation = function (event, slick) {

		var slides = $('.slide'),
		    activeSlide = $('.slick-current'),
		    activeContent = activeSlide.find('.hero-content'),


		// This is a string like so: 'animated someCssClass'.
		animationClass = activeContent.attr('data-animation'),
		    splitAnimation = animationClass.split(' '),


		// This is the 'animated' class.
		animationTrigger = splitAnimation[0],


		// This is the animate.css class.
		animateCss = splitAnimation[1];

		// Go through each slide to see if we've already set animation classes.
		slides.each(function (index, element) {

			var slideContent = $(this).find('.hero-content');

			// If we've set animation classes on a slide, remove them.
			if (slideContent.hasClass('animated')) {

				// Get the last class, which is the animate.css class.
				var lastClass = slideContent.attr('class').split(' ').pop();

				// Remove both animation classes.
				slideContent.removeClass(lastClass).removeClass(animationTrigger);
			}
		});

		// Add animation classes after slide is in view.
		activeContent.addClass(animationClass);
	};

	// Allow background videos to autoplay.
	app.playBackgroundVideos = function () {

		// Get all the videos in our slides object.
		$('video').each(function () {

			// Let them autoplay. TODO: Possibly change this later to only play the visible slide video.
			this.play();
		});
	};

	// Kick off Slick.
	app.doSlick = function () {

		app.$c.heroCarousel.on('init', app.playBackgroundVideos);

		app.$c.heroCarousel.slick({
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: false,
			dots: false,
			focusOnSelect: true,
			waitForAnimate: true
		});

		app.$c.heroCarousel.on('afterChange', app.doAnimation);
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsHeroCarousel);
'use strict';

/**
 * File js-enabled.js
 *
 * If Javascript is enabled, replace the <body> class "no-js".
 */
document.body.className = document.body.className.replace('no-js', 'js');
"use strict";

// (function(){

window.menuAnimation = {};
(function (window, TweenMax, $, app) {
    console.log("Menu animation");

    // console.log(TweenMax);
    //   console.log(Tweenmax);
})(window, TweenMax, jQuery, window.menuAnimation);
"use strict";

// (function(){

Barba.Pjax.start();
window.menuTrigger = {};
var menuPositionTop;
var menuPositionLeft;
var navWidth;
(function (window, $, TweenMax, app) {
  $(function () {
    var menuComplete = function menuComplete() {
      console.log("menu complete");
      menuPositionTop = $("#primary-menu").offset().top;
      menuPositionLeft = $("#primary-menu").offset().left;
      navWidth = $('#site-navigation').width();
      console.log("nav width is : " + navWidth);
      console.log(menuPositionTop + " . " + menuPositionLeft);
      // return menuPositionLeft;
    };
    menuComplete();

    TweenMax.set("svg#logotipo-svg-logo", { scale: 1 });

    // console.log(menuPositionTop + " . " + menuPositionLeft);
    var Anim = TweenMax.to("#site-navigation", .4, { x: "0%", reversed: true, paused: true, ease: Power2.easeInOut, onComplete: menuComplete });
    var AnimAfter = TweenMax.to("#site-navigation::after", .4, { x: "0%", reversed: true, paused: true, ease: Power2.easeInOut }, 3);
    var AnimBorder = TweenMax.to("#borders .top, #borders .bottom", .4, { height: "70px", reversed: true, paused: true, ease: Power2.easeInOut });
    var AnimLogo = TweenMax.to("svg#logotipo-svg-logo", .5, { x: -menuPositionLeft / 3, y: menuPositionTop / 2, scale: 2, reversed: true, paused: true, ease: Power2.easeInOut });

    $('.menu-trigger').on('click', function () {
      $(this).toggleClass('active');
      Anim.reversed() ? Anim.play() : Anim.reverse();
      AnimAfter.reversed() ? AnimAfter.play() : AnimAfter.reverse();
      AnimBorder.reversed() ? AnimBorder.play() : AnimBorder.reverse();
      AnimLogo.reversed() ? AnimLogo.play() : AnimLogo.reverse();
      return false;
    });
  });
  console.log("trigger");
})(window, jQuery, TweenMax, window.menuTrigger);
'use strict';

/**
 * File: mobile-menu.js
 *
 * Create an accordion style dropdown.
 */
window.wdsMobileMenu = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			subMenuContainer: $('.mobile-menu .sub-menu'),
			subMenuParentItem: $('.mobile-menu li.menu-item-has-children'),
			offCanvasContainer: $('.off-canvas-container')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.addDownArrow);
		app.$c.subMenuParentItem.on('click', app.toggleSubmenu);
		app.$c.subMenuParentItem.on('transitionend', app.resetSubMenu);
		app.$c.offCanvasContainer.on('transitionend', app.forceCloseSubmenus);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.subMenuContainer.length;
	};

	// Reset the submenus after it's done closing.
	app.resetSubMenu = function (e) {
		var $target = $(e.target);

		// When the list item is done transitioning in height,
		// remove the classes from the submenu so it is ready to toggle again.
		if ($target.is('li.menu-item-has-children') && !$target.hasClass('is-visible')) {
			$target.find('ul.sub-menu').removeClass('slideOutLeft is-visible');
		}
	};

	// Slide out the submenu items.
	app.slideOutSubMenus = function () {
		app.$c.subMenuContainer.each(function () {

			// Only try to close submenus that are actually open.
			if ($(this).hasClass('slideInLeft')) {

				// Close the parent list item, and set the corresponding button aria to false.
				$(this).parent().removeClass('is-visible').find('.parent-indicator').attr('aria-expanded', false);

				// Slide out the submenu.
				$(this).removeClass('slideInLeft').addClass('slideOutLeft');
			}
		});
	};

	// Add the down arrow to submenu parents.
	app.addDownArrow = function () {
		app.$c.subMenuParentItem.prepend('<button type="button" aria-expanded="false" class="parent-indicator" aria-label="Open submenu"><span class="down-arrow"></span></button>');
	};

	// Deal with the submenu.
	app.toggleSubmenu = function (e) {

		var el = $(this),
		    // The menu element which was clicked on.
		subMenu = el.children('ul.sub-menu'),
		    // The nearest submenu.
		$target = $(e.target); // the element that's actually being clicked (child of the li that triggered the click event).

		// Figure out if we're clicking the button or its arrow child,
		// if so, we can just open or close the menu and bail.
		if ($target.hasClass('down-arrow') || $target.hasClass('parent-indicator')) {

			// First, collapse any already opened submenus.
			app.slideOutSubMenus();

			if (!subMenu.hasClass('is-visible')) {

				// Open the submenu.
				app.openSubmenu(el, subMenu);
			}

			return false;
		}
	};

	// Open a submenu.
	app.openSubmenu = function (parent, subMenu) {

		// Expand the list menu item, and set the corresponding button aria to true.
		parent.addClass('is-visible').find('.parent-indicator').attr('aria-expanded', true);

		// Slide the menu in.
		subMenu.addClass('is-visible animated slideInLeft');
	};

	// Force close all the submenus when the main menu container is closed.
	app.forceCloseSubmenus = function () {

		// The transitionend event triggers on open and on close, need to make sure we only do this on close.
		if (!$(this).hasClass('is-visible')) {
			app.$c.subMenuParentItem.removeClass('is-visible').find('.parent-indicator').attr('aria-expanded', false);
			app.$c.subMenuContainer.removeClass('is-visible slideInLeft');
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsMobileMenu);
'use strict';

/**
 * File modal.js
 *
 * Deal with multiple modals and their media.
 */
window.wdsModal = {};
(function (window, $, app) {

	var $modalToggle = void 0,
	    $focusableChildren = void 0,
	    $player = void 0,
	    $tag = document.createElement('script'),
	    $firstScriptTag = document.getElementsByTagName('script')[0],
	    YT = void 0;

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			$firstScriptTag.parentNode.insertBefore($tag, $firstScriptTag);
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			'body': $('body')
		};
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return $('.modal-trigger').length;
	};

	// Combine all events.
	app.bindEvents = function () {

		// Trigger a modal to open.
		app.$c.body.on('click touchstart', '.modal-trigger', app.openModal);

		// Trigger the close button to close the modal.
		app.$c.body.on('click touchstart', '.close', app.closeModal);

		// Allow the user to close the modal by hitting the esc key.
		app.$c.body.on('keydown', app.escKeyClose);

		// Allow the user to close the modal by clicking outside of the modal.
		app.$c.body.on('click touchstart', 'div.modal-open', app.closeModalByClick);

		// Listen to tabs, trap keyboard if we need to
		app.$c.body.on('keydown', app.trapKeyboardMaybe);
	};

	// Open the modal.
	app.openModal = function () {

		// Store the modal toggle element
		$modalToggle = $(this);

		// Figure out which modal we're opening and store the object.
		var $modal = $($(this).data('target'));

		// Display the modal.
		$modal.addClass('modal-open');

		// Add body class.
		app.$c.body.addClass('modal-open');

		// Find the focusable children of the modal.
		// This list may be incomplete, really wish jQuery had the :focusable pseudo like jQuery UI does.
		// For more about :input see: https://api.jquery.com/input-selector/
		$focusableChildren = $modal.find('a, :input, [tabindex]');

		// Ideally, there is always one (the close button), but you never know.
		if (0 < $focusableChildren.length) {

			// Shift focus to the first focusable element.
			$focusableChildren[0].focus();
		}
	};

	// Close the modal.
	app.closeModal = function () {

		// Figure the opened modal we're closing and store the object.
		var $modal = $($('div.modal-open .close').data('target')),


		// Find the iframe in the $modal object.
		$iframe = $modal.find('iframe');

		// Only do this if there are any iframes.
		if ($iframe.length) {

			// Get the iframe src URL.
			var url = $iframe.attr('src');

			// Removing/Readding the URL will effectively break the YouTube API.
			// So let's not do that when the iframe URL contains the enablejsapi parameter.
			if (!url.includes('enablejsapi=1')) {

				// Remove the source URL, then add it back, so the video can be played again later.
				$iframe.attr('src', '').attr('src', url);
			} else {

				// Use the YouTube API to stop the video.
				$player.stopVideo();
			}
		}

		// Finally, hide the modal.
		$modal.removeClass('modal-open');

		// Remove the body class.
		app.$c.body.removeClass('modal-open');

		// Revert focus back to toggle element
		$modalToggle.focus();
	};

	// Close if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeModal();
		}
	};

	// Close if the user clicks outside of the modal
	app.closeModalByClick = function (event) {

		// If the parent container is NOT the modal dialog container, close the modal
		if (!$(event.target).parents('div').hasClass('modal-dialog')) {
			app.closeModal();
		}
	};

	// Trap the keyboard into a modal when one is active.
	app.trapKeyboardMaybe = function (event) {

		// We only need to do stuff when the modal is open and tab is pressed.
		if (9 === event.which && 0 < $('.modal-open').length) {
			var $focused = $(':focus'),
			    focusIndex = $focusableChildren.index($focused);

			if (0 === focusIndex && event.shiftKey) {

				// If this is the first focusable element, and shift is held when pressing tab, go back to last focusable element.
				$focusableChildren[$focusableChildren.length - 1].focus();
				event.preventDefault();
			} else if (!event.shiftKey && focusIndex === $focusableChildren.length - 1) {

				// If this is the last focusable element, and shift is not held, go back to the first focusable element.
				$focusableChildren[0].focus();
				event.preventDefault();
			}
		}
	};

	// Hook into YouTube <iframe>.
	app.onYouTubeIframeAPIReady = function () {
		var $modal = $('div.modal'),
		    $iframeid = $modal.find('iframe').attr('id');

		$player = new YT.Player($iframeid, {
			events: {
				'onReady': app.onPlayerReady,
				'onStateChange': app.onPlayerStateChange
			}
		});
	};

	// Do something on player ready.
	app.onPlayerReady = function () {};

	// Do something on player state change.
	app.onPlayerStateChange = function () {

		// Set focus to the first focusable element inside of the modal the player is in.
		$(event.target.a).parents('.modal').find('a, :input, [tabindex]').first().focus();
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsModal);
'use strict';

/**
 * File: navigation-primary.js
 *
 * Helpers for the primary navigation.
 */
window.wdsPrimaryNavigation = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			subMenuContainer: $('.main-navigation .sub-menu'),
			subMenuParentItem: $('.main-navigation li.menu-item-has-children')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.addDownArrow);
		app.$c.subMenuParentItem.find('a').on('focusin focusout', app.toggleFocus);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.subMenuContainer.length;
	};

	// Add the down arrow to submenu parents.
	app.addDownArrow = function () {
		app.$c.subMenuParentItem.find('> a').append('<span class="caret-down" aria-hidden="true"></span>');
	};

	// Toggle the focus class on the link parent.
	app.toggleFocus = function () {
		$(this).parents('li.menu-item-has-children').toggleClass('focus');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsPrimaryNavigation);
'use strict';

/**
 * File: off-canvas.js
 *
 * Help deal with the off-canvas mobile menu.
 */
window.wdsoffCanvas = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			body: $('body'),
			offCanvasClose: $('.off-canvas-close'),
			offCanvasContainer: $('.off-canvas-container'),
			offCanvasOpen: $('.off-canvas-open'),
			offCanvasScreen: $('.off-canvas-screen')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.body.on('keydown', app.escKeyClose);
		app.$c.offCanvasClose.on('click', app.closeoffCanvas);
		app.$c.offCanvasOpen.on('click', app.toggleoffCanvas);
		app.$c.offCanvasScreen.on('click', app.closeoffCanvas);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.offCanvasContainer.length;
	};

	// To show or not to show?
	app.toggleoffCanvas = function () {

		if ('true' === $(this).attr('aria-expanded')) {
			app.closeoffCanvas();
		} else {
			app.openoffCanvas();
		}
	};

	// Show that drawer!
	app.openoffCanvas = function () {
		app.$c.offCanvasContainer.addClass('is-visible');
		app.$c.offCanvasOpen.addClass('is-visible');
		app.$c.offCanvasScreen.addClass('is-visible');

		app.$c.offCanvasOpen.attr('aria-expanded', true);
		app.$c.offCanvasContainer.attr('aria-hidden', false);

		app.$c.offCanvasContainer.find('button').first().focus();
	};

	// Close that drawer!
	app.closeoffCanvas = function () {
		app.$c.offCanvasContainer.removeClass('is-visible');
		app.$c.offCanvasOpen.removeClass('is-visible');
		app.$c.offCanvasScreen.removeClass('is-visible');

		app.$c.offCanvasOpen.attr('aria-expanded', false);
		app.$c.offCanvasContainer.attr('aria-hidden', true);

		app.$c.offCanvasOpen.focus();
	};

	// Close drawer if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeoffCanvas();
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsoffCanvas);
'use strict';

/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
(function () {
	var isWebkit = -1 < navigator.userAgent.toLowerCase().indexOf('webkit'),
	    isOpera = -1 < navigator.userAgent.toLowerCase().indexOf('opera'),
	    isIe = -1 < navigator.userAgent.toLowerCase().indexOf('msie');

	if ((isWebkit || isOpera || isIe) && document.getElementById && window.addEventListener) {
		window.addEventListener('hashchange', function () {
			var id = location.hash.substring(1),
			    element;

			if (!/^[A-z0-9_-]+$/.test(id)) {
				return;
			}

			element = document.getElementById(id);

			if (element) {
				if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false);
	}
})();
'use strict';

/**
 * File window-ready.js
 *
 * Add a "ready" class to <body> when window is ready.
 */
window.wdsWindowReady = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();
		app.bindEvents();
	};

	// Cache document elements.
	app.cache = function () {
		app.$c = {
			'window': $(window),
			'body': $(document.body)
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.load(app.addBodyClass);
	};

	// Add a class to <body>.
	app.addBodyClass = function () {
		app.$c.body.addClass('ready');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsWindowReady);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhlcm8tY2Fyb3VzZWwuanMiLCJqcy1lbmFibGVkLmpzIiwibWVudS1hbmltYXRpb24uanMiLCJtZW51LXRyaWdnZXIuanMiLCJtb2JpbGUtbWVudS5qcyIsIm1vZGFsLmpzIiwibmF2aWdhdGlvbi1wcmltYXJ5LmpzIiwib2ZmLWNhbnZhcy5qcyIsInNraXAtbGluay1mb2N1cy1maXguanMiLCJ3aW5kb3ctcmVhZHkuanMiXSwibmFtZXMiOlsid2luZG93Iiwid2RzSGVyb0Nhcm91c2VsIiwiJCIsImFwcCIsImluaXQiLCJjYWNoZSIsIm1lZXRzUmVxdWlyZW1lbnRzIiwiYmluZEV2ZW50cyIsIiRjIiwiaGVyb0Nhcm91c2VsIiwib24iLCJkb1NsaWNrIiwiZG9GaXJzdEFuaW1hdGlvbiIsImxlbmd0aCIsImZpcnN0U2xpZGUiLCJmaW5kIiwiZmlyc3RTbGlkZUNvbnRlbnQiLCJmaXJzdEFuaW1hdGlvbiIsImF0dHIiLCJhZGRDbGFzcyIsImRvQW5pbWF0aW9uIiwiZXZlbnQiLCJzbGljayIsInNsaWRlcyIsImFjdGl2ZVNsaWRlIiwiYWN0aXZlQ29udGVudCIsImFuaW1hdGlvbkNsYXNzIiwic3BsaXRBbmltYXRpb24iLCJzcGxpdCIsImFuaW1hdGlvblRyaWdnZXIiLCJhbmltYXRlQ3NzIiwiZWFjaCIsImluZGV4IiwiZWxlbWVudCIsInNsaWRlQ29udGVudCIsImhhc0NsYXNzIiwibGFzdENsYXNzIiwicG9wIiwicmVtb3ZlQ2xhc3MiLCJwbGF5QmFja2dyb3VuZFZpZGVvcyIsInBsYXkiLCJhdXRvcGxheSIsImF1dG9wbGF5U3BlZWQiLCJhcnJvd3MiLCJkb3RzIiwiZm9jdXNPblNlbGVjdCIsIndhaXRGb3JBbmltYXRlIiwialF1ZXJ5IiwiZG9jdW1lbnQiLCJib2R5IiwiY2xhc3NOYW1lIiwicmVwbGFjZSIsIm1lbnVBbmltYXRpb24iLCJUd2Vlbk1heCIsImNvbnNvbGUiLCJsb2ciLCJCYXJiYSIsIlBqYXgiLCJzdGFydCIsIm1lbnVUcmlnZ2VyIiwibWVudVBvc2l0aW9uVG9wIiwibWVudVBvc2l0aW9uTGVmdCIsIm5hdldpZHRoIiwibWVudUNvbXBsZXRlIiwib2Zmc2V0IiwidG9wIiwibGVmdCIsIndpZHRoIiwic2V0Iiwic2NhbGUiLCJBbmltIiwidG8iLCJ4IiwicmV2ZXJzZWQiLCJwYXVzZWQiLCJlYXNlIiwiUG93ZXIyIiwiZWFzZUluT3V0Iiwib25Db21wbGV0ZSIsIkFuaW1BZnRlciIsIkFuaW1Cb3JkZXIiLCJoZWlnaHQiLCJBbmltTG9nbyIsInkiLCJ0b2dnbGVDbGFzcyIsInJldmVyc2UiLCJ3ZHNNb2JpbGVNZW51Iiwic3ViTWVudUNvbnRhaW5lciIsInN1Yk1lbnVQYXJlbnRJdGVtIiwib2ZmQ2FudmFzQ29udGFpbmVyIiwiYWRkRG93bkFycm93IiwidG9nZ2xlU3VibWVudSIsInJlc2V0U3ViTWVudSIsImZvcmNlQ2xvc2VTdWJtZW51cyIsImUiLCIkdGFyZ2V0IiwidGFyZ2V0IiwiaXMiLCJzbGlkZU91dFN1Yk1lbnVzIiwicGFyZW50IiwicHJlcGVuZCIsImVsIiwic3ViTWVudSIsImNoaWxkcmVuIiwib3BlblN1Ym1lbnUiLCJ3ZHNNb2RhbCIsIiRtb2RhbFRvZ2dsZSIsIiRmb2N1c2FibGVDaGlsZHJlbiIsIiRwbGF5ZXIiLCIkdGFnIiwiY3JlYXRlRWxlbWVudCIsIiRmaXJzdFNjcmlwdFRhZyIsImdldEVsZW1lbnRzQnlUYWdOYW1lIiwiWVQiLCJwYXJlbnROb2RlIiwiaW5zZXJ0QmVmb3JlIiwib3Blbk1vZGFsIiwiY2xvc2VNb2RhbCIsImVzY0tleUNsb3NlIiwiY2xvc2VNb2RhbEJ5Q2xpY2siLCJ0cmFwS2V5Ym9hcmRNYXliZSIsIiRtb2RhbCIsImRhdGEiLCJmb2N1cyIsIiRpZnJhbWUiLCJ1cmwiLCJpbmNsdWRlcyIsInN0b3BWaWRlbyIsImtleUNvZGUiLCJwYXJlbnRzIiwid2hpY2giLCIkZm9jdXNlZCIsImZvY3VzSW5kZXgiLCJzaGlmdEtleSIsInByZXZlbnREZWZhdWx0Iiwib25Zb3VUdWJlSWZyYW1lQVBJUmVhZHkiLCIkaWZyYW1laWQiLCJQbGF5ZXIiLCJldmVudHMiLCJvblBsYXllclJlYWR5Iiwib25QbGF5ZXJTdGF0ZUNoYW5nZSIsImEiLCJmaXJzdCIsIndkc1ByaW1hcnlOYXZpZ2F0aW9uIiwidG9nZ2xlRm9jdXMiLCJhcHBlbmQiLCJ3ZHNvZmZDYW52YXMiLCJvZmZDYW52YXNDbG9zZSIsIm9mZkNhbnZhc09wZW4iLCJvZmZDYW52YXNTY3JlZW4iLCJjbG9zZW9mZkNhbnZhcyIsInRvZ2dsZW9mZkNhbnZhcyIsIm9wZW5vZmZDYW52YXMiLCJpc1dlYmtpdCIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsInRvTG93ZXJDYXNlIiwiaW5kZXhPZiIsImlzT3BlcmEiLCJpc0llIiwiZ2V0RWxlbWVudEJ5SWQiLCJhZGRFdmVudExpc3RlbmVyIiwiaWQiLCJsb2NhdGlvbiIsImhhc2giLCJzdWJzdHJpbmciLCJ0ZXN0IiwidGFnTmFtZSIsInRhYkluZGV4Iiwid2RzV2luZG93UmVhZHkiLCJsb2FkIiwiYWRkQm9keUNsYXNzIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7OztBQUtBQSxPQUFPQyxlQUFQLEdBQXlCLEVBQXpCO0FBQ0UsV0FBVUQsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTJCOztBQUU1QjtBQUNBQSxLQUFJQyxJQUFKLEdBQVcsWUFBVztBQUNyQkQsTUFBSUUsS0FBSjs7QUFFQSxNQUFLRixJQUFJRyxpQkFBSixFQUFMLEVBQStCO0FBQzlCSCxPQUFJSSxVQUFKO0FBQ0E7QUFDRCxFQU5EOztBQVFBO0FBQ0FKLEtBQUlFLEtBQUosR0FBWSxZQUFXO0FBQ3RCRixNQUFJSyxFQUFKLEdBQVM7QUFDUlIsV0FBUUUsRUFBR0YsTUFBSCxDQURBO0FBRVJTLGlCQUFjUCxFQUFHLFdBQUg7QUFGTixHQUFUO0FBSUEsRUFMRDs7QUFPQTtBQUNBQyxLQUFJSSxVQUFKLEdBQWlCLFlBQVc7QUFDM0JKLE1BQUlLLEVBQUosQ0FBT1IsTUFBUCxDQUFjVSxFQUFkLENBQWtCLE1BQWxCLEVBQTBCUCxJQUFJUSxPQUE5QjtBQUNBUixNQUFJSyxFQUFKLENBQU9SLE1BQVAsQ0FBY1UsRUFBZCxDQUFrQixNQUFsQixFQUEwQlAsSUFBSVMsZ0JBQTlCO0FBRUEsRUFKRDs7QUFNQTtBQUNBVCxLQUFJRyxpQkFBSixHQUF3QixZQUFXO0FBQ2xDLFNBQU9ILElBQUlLLEVBQUosQ0FBT0MsWUFBUCxDQUFvQkksTUFBM0I7QUFDQSxFQUZEOztBQUlBO0FBQ0FWLEtBQUlTLGdCQUFKLEdBQXVCLFlBQVc7O0FBRWpDO0FBQ0EsTUFBSUUsYUFBYVgsSUFBSUssRUFBSixDQUFPQyxZQUFQLENBQW9CTSxJQUFwQixDQUEwQixzQkFBMUIsQ0FBakI7QUFBQSxNQUNDQyxvQkFBb0JGLFdBQVdDLElBQVgsQ0FBaUIsZUFBakIsQ0FEckI7QUFBQSxNQUVDRSxpQkFBaUJELGtCQUFrQkUsSUFBbEIsQ0FBd0IsZ0JBQXhCLENBRmxCOztBQUlBO0FBQ0FGLG9CQUFrQkcsUUFBbEIsQ0FBNEJGLGNBQTVCO0FBQ0EsRUFURDs7QUFXQTtBQUNBZCxLQUFJaUIsV0FBSixHQUFrQixVQUFVQyxLQUFWLEVBQWlCQyxLQUFqQixFQUF5Qjs7QUFFMUMsTUFBSUMsU0FBU3JCLEVBQUcsUUFBSCxDQUFiO0FBQUEsTUFDQ3NCLGNBQWN0QixFQUFHLGdCQUFILENBRGY7QUFBQSxNQUVDdUIsZ0JBQWdCRCxZQUFZVCxJQUFaLENBQWtCLGVBQWxCLENBRmpCOzs7QUFJQztBQUNBVyxtQkFBaUJELGNBQWNQLElBQWQsQ0FBb0IsZ0JBQXBCLENBTGxCO0FBQUEsTUFNQ1MsaUJBQWlCRCxlQUFlRSxLQUFmLENBQXNCLEdBQXRCLENBTmxCOzs7QUFRQztBQUNBQyxxQkFBbUJGLGVBQWUsQ0FBZixDQVRwQjs7O0FBV0E7QUFDQUcsZUFBYUgsZUFBZSxDQUFmLENBWmI7O0FBY0E7QUFDQUosU0FBT1EsSUFBUCxDQUFhLFVBQVVDLEtBQVYsRUFBaUJDLE9BQWpCLEVBQTJCOztBQUV2QyxPQUFJQyxlQUFlaEMsRUFBRyxJQUFILEVBQVVhLElBQVYsQ0FBZ0IsZUFBaEIsQ0FBbkI7O0FBRUE7QUFDQSxPQUFLbUIsYUFBYUMsUUFBYixDQUF1QixVQUF2QixDQUFMLEVBQTJDOztBQUUxQztBQUNBLFFBQUlDLFlBQVlGLGFBQWFoQixJQUFiLENBQW1CLE9BQW5CLEVBQTZCVSxLQUE3QixDQUFvQyxHQUFwQyxFQUEwQ1MsR0FBMUMsRUFBaEI7O0FBRUE7QUFDQUgsaUJBQWFJLFdBQWIsQ0FBMEJGLFNBQTFCLEVBQXNDRSxXQUF0QyxDQUFtRFQsZ0JBQW5EO0FBQ0E7QUFDRCxHQWJEOztBQWVBO0FBQ0FKLGdCQUFjTixRQUFkLENBQXdCTyxjQUF4QjtBQUNBLEVBbENEOztBQW9DQTtBQUNBdkIsS0FBSW9DLG9CQUFKLEdBQTJCLFlBQVc7O0FBRXJDO0FBQ0FyQyxJQUFHLE9BQUgsRUFBYTZCLElBQWIsQ0FBbUIsWUFBVzs7QUFFN0I7QUFDQSxRQUFLUyxJQUFMO0FBQ0EsR0FKRDtBQUtBLEVBUkQ7O0FBVUE7QUFDQXJDLEtBQUlRLE9BQUosR0FBYyxZQUFXOztBQUV4QlIsTUFBSUssRUFBSixDQUFPQyxZQUFQLENBQW9CQyxFQUFwQixDQUF3QixNQUF4QixFQUFnQ1AsSUFBSW9DLG9CQUFwQzs7QUFFQXBDLE1BQUlLLEVBQUosQ0FBT0MsWUFBUCxDQUFvQmEsS0FBcEIsQ0FBMEI7QUFDekJtQixhQUFVLElBRGU7QUFFekJDLGtCQUFlLElBRlU7QUFHekJDLFdBQVEsS0FIaUI7QUFJekJDLFNBQU0sS0FKbUI7QUFLekJDLGtCQUFlLElBTFU7QUFNekJDLG1CQUFnQjtBQU5TLEdBQTFCOztBQVNBM0MsTUFBSUssRUFBSixDQUFPQyxZQUFQLENBQW9CQyxFQUFwQixDQUF3QixhQUF4QixFQUF1Q1AsSUFBSWlCLFdBQTNDO0FBQ0EsRUFkRDs7QUFnQkE7QUFDQWxCLEdBQUdDLElBQUlDLElBQVA7QUFFQSxDQS9HQyxFQStHQ0osTUEvR0QsRUErR1MrQyxNQS9HVCxFQStHaUIvQyxPQUFPQyxlQS9HeEIsQ0FBRjs7O0FDTkE7Ozs7O0FBS0ErQyxTQUFTQyxJQUFULENBQWNDLFNBQWQsR0FBMEJGLFNBQVNDLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkMsT0FBeEIsQ0FBaUMsT0FBakMsRUFBMEMsSUFBMUMsQ0FBMUI7OztBQ0pBOztBQUVBbkQsT0FBT29ELGFBQVAsR0FBdUIsRUFBdkI7QUFDRSxXQUFVcEQsTUFBVixFQUFrQnFELFFBQWxCLEVBQTRCbkQsQ0FBNUIsRUFBK0JDLEdBQS9CLEVBQXFDO0FBQ25DbUQsWUFBUUMsR0FBUixDQUFZLGdCQUFaOztBQUVKO0FBQ0k7QUFDRSxDQUxKLEVBS0t2RCxNQUxMLEVBS2FxRCxRQUxiLEVBS3dCTixNQUx4QixFQUtnQy9DLE9BQU9vRCxhQUx2QyxDQUFGOzs7QUNIQTs7QUFFQUksTUFBTUMsSUFBTixDQUFXQyxLQUFYO0FBQ0ExRCxPQUFPMkQsV0FBUCxHQUFxQixFQUFyQjtBQUNFLElBQUlDLGVBQUo7QUFDQSxJQUFJQyxnQkFBSjtBQUNBLElBQUlDLFFBQUo7QUFDQSxXQUFVOUQsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJtRCxRQUFyQixFQUErQmxELEdBQS9CLEVBQXFDO0FBQ3ZDRCxJQUFFLFlBQVU7QUFDVixRQUFJNkQsZUFBZSxTQUFmQSxZQUFlLEdBQU07QUFDdkJULGNBQVFDLEdBQVIsQ0FBWSxlQUFaO0FBQ0NLLHdCQUFrQjFELEVBQUUsZUFBRixFQUFtQjhELE1BQW5CLEdBQTRCQyxHQUE5QztBQUNBSix5QkFBbUIzRCxFQUFFLGVBQUYsRUFBbUI4RCxNQUFuQixHQUE0QkUsSUFBL0M7QUFDQUosaUJBQVc1RCxFQUFFLGtCQUFGLEVBQXNCaUUsS0FBdEIsRUFBWDtBQUNBYixjQUFRQyxHQUFSLENBQVksb0JBQW9CTyxRQUFoQztBQUNFUixjQUFRQyxHQUFSLENBQVlLLGtCQUFrQixLQUFsQixHQUEwQkMsZ0JBQXRDO0FBQ0Q7QUFDSCxLQVJEO0FBU0FFOztBQUVJVixhQUFTZSxHQUFULENBQWEsdUJBQWIsRUFBcUMsRUFBQ0MsT0FBTSxDQUFQLEVBQXJDOztBQUVBO0FBQ0EsUUFBSUMsT0FBT2pCLFNBQVNrQixFQUFULENBQWEsa0JBQWIsRUFBa0MsRUFBbEMsRUFBdUMsRUFBRUMsR0FBRSxJQUFKLEVBQVdDLFVBQVMsSUFBcEIsRUFBMEJDLFFBQU8sSUFBakMsRUFBdUNDLE1BQU1DLE9BQU9DLFNBQXBELEVBQStEQyxZQUFZZixZQUEzRSxFQUF2QyxDQUFYO0FBQ0EsUUFBSWdCLFlBQVkxQixTQUFTa0IsRUFBVCxDQUFhLHlCQUFiLEVBQXlDLEVBQXpDLEVBQThDLEVBQUVDLEdBQUUsSUFBSixFQUFXQyxVQUFTLElBQXBCLEVBQTBCQyxRQUFPLElBQWpDLEVBQXVDQyxNQUFNQyxPQUFPQyxTQUFwRCxFQUE5QyxFQUE4RyxDQUE5RyxDQUFoQjtBQUNBLFFBQUlHLGFBQWEzQixTQUFTa0IsRUFBVCxDQUFhLGlDQUFiLEVBQWlELEVBQWpELEVBQXNELEVBQUVVLFFBQU8sTUFBVCxFQUFrQlIsVUFBUyxJQUEzQixFQUFpQ0MsUUFBTyxJQUF4QyxFQUE4Q0MsTUFBTUMsT0FBT0MsU0FBM0QsRUFBdEQsQ0FBakI7QUFDQSxRQUFJSyxXQUFXN0IsU0FBU2tCLEVBQVQsQ0FBYSx1QkFBYixFQUF1QyxFQUF2QyxFQUE0QyxFQUFFQyxHQUFFLENBQUNYLGdCQUFELEdBQW9CLENBQXhCLEVBQTBCc0IsR0FBRXZCLGtCQUFrQixDQUE5QyxFQUFnRFMsT0FBTSxDQUF0RCxFQUEwREksVUFBUyxJQUFuRSxFQUF5RUMsUUFBTyxJQUFoRixFQUFzRkMsTUFBTUMsT0FBT0MsU0FBbkcsRUFBNUMsQ0FBZjs7QUFFRjNFLE1BQUUsZUFBRixFQUFtQlEsRUFBbkIsQ0FBc0IsT0FBdEIsRUFBK0IsWUFBVztBQUN4Q1IsUUFBRSxJQUFGLEVBQVFrRixXQUFSLENBQW9CLFFBQXBCO0FBQ0FkLFdBQUtHLFFBQUwsS0FBa0JILEtBQUs5QixJQUFMLEVBQWxCLEdBQWdDOEIsS0FBS2UsT0FBTCxFQUFoQztBQUNBTixnQkFBVU4sUUFBVixLQUF1Qk0sVUFBVXZDLElBQVYsRUFBdkIsR0FBMEN1QyxVQUFVTSxPQUFWLEVBQTFDO0FBQ0FMLGlCQUFXUCxRQUFYLEtBQXdCTyxXQUFXeEMsSUFBWCxFQUF4QixHQUE0Q3dDLFdBQVdLLE9BQVgsRUFBNUM7QUFDQUgsZUFBU1QsUUFBVCxLQUFzQlMsU0FBUzFDLElBQVQsRUFBdEIsR0FBd0MwQyxTQUFTRyxPQUFULEVBQXhDO0FBQ0EsYUFBTyxLQUFQO0FBQ0QsS0FQRDtBQVFELEdBNUJIO0FBNkJJL0IsVUFBUUMsR0FBUixDQUFZLFNBQVo7QUFDRSxDQS9CSixFQStCS3ZELE1BL0JMLEVBK0JhK0MsTUEvQmIsRUErQnFCTSxRQS9CckIsRUErQitCckQsT0FBTzJELFdBL0J0QyxDQUFGOzs7QUNSQTs7Ozs7QUFLQTNELE9BQU9zRixhQUFQLEdBQXVCLEVBQXZCO0FBQ0UsV0FBVXRGLE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEyQjs7QUFFNUI7QUFDQUEsS0FBSUMsSUFBSixHQUFXLFlBQVc7QUFDckJELE1BQUlFLEtBQUo7O0FBRUEsTUFBS0YsSUFBSUcsaUJBQUosRUFBTCxFQUErQjtBQUM5QkgsT0FBSUksVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBSixLQUFJRSxLQUFKLEdBQVksWUFBVztBQUN0QkYsTUFBSUssRUFBSixHQUFTO0FBQ1JSLFdBQVFFLEVBQUdGLE1BQUgsQ0FEQTtBQUVSdUYscUJBQWtCckYsRUFBRyx3QkFBSCxDQUZWO0FBR1JzRixzQkFBbUJ0RixFQUFHLHdDQUFILENBSFg7QUFJUnVGLHVCQUFvQnZGLEVBQUcsdUJBQUg7QUFKWixHQUFUO0FBTUEsRUFQRDs7QUFTQTtBQUNBQyxLQUFJSSxVQUFKLEdBQWlCLFlBQVc7QUFDM0JKLE1BQUlLLEVBQUosQ0FBT1IsTUFBUCxDQUFjVSxFQUFkLENBQWtCLE1BQWxCLEVBQTBCUCxJQUFJdUYsWUFBOUI7QUFDQXZGLE1BQUlLLEVBQUosQ0FBT2dGLGlCQUFQLENBQXlCOUUsRUFBekIsQ0FBNkIsT0FBN0IsRUFBc0NQLElBQUl3RixhQUExQztBQUNBeEYsTUFBSUssRUFBSixDQUFPZ0YsaUJBQVAsQ0FBeUI5RSxFQUF6QixDQUE2QixlQUE3QixFQUE4Q1AsSUFBSXlGLFlBQWxEO0FBQ0F6RixNQUFJSyxFQUFKLENBQU9pRixrQkFBUCxDQUEwQi9FLEVBQTFCLENBQThCLGVBQTlCLEVBQStDUCxJQUFJMEYsa0JBQW5EO0FBQ0EsRUFMRDs7QUFPQTtBQUNBMUYsS0FBSUcsaUJBQUosR0FBd0IsWUFBVztBQUNsQyxTQUFPSCxJQUFJSyxFQUFKLENBQU8rRSxnQkFBUCxDQUF3QjFFLE1BQS9CO0FBQ0EsRUFGRDs7QUFJQTtBQUNBVixLQUFJeUYsWUFBSixHQUFtQixVQUFVRSxDQUFWLEVBQWM7QUFDaEMsTUFBTUMsVUFBVTdGLEVBQUc0RixFQUFFRSxNQUFMLENBQWhCOztBQUVBO0FBQ0E7QUFDQSxNQUFLRCxRQUFRRSxFQUFSLENBQVksMkJBQVosS0FBNkMsQ0FBRUYsUUFBUTVELFFBQVIsQ0FBa0IsWUFBbEIsQ0FBcEQsRUFBdUY7QUFDdEY0RCxXQUFRaEYsSUFBUixDQUFjLGFBQWQsRUFBOEJ1QixXQUE5QixDQUEyQyx5QkFBM0M7QUFDQTtBQUVELEVBVEQ7O0FBV0E7QUFDQW5DLEtBQUkrRixnQkFBSixHQUF1QixZQUFXO0FBQ2pDL0YsTUFBSUssRUFBSixDQUFPK0UsZ0JBQVAsQ0FBd0J4RCxJQUF4QixDQUE4QixZQUFXOztBQUV4QztBQUNBLE9BQUs3QixFQUFHLElBQUgsRUFBVWlDLFFBQVYsQ0FBb0IsYUFBcEIsQ0FBTCxFQUEyQzs7QUFFMUM7QUFDQWpDLE1BQUcsSUFBSCxFQUFVaUcsTUFBVixHQUFtQjdELFdBQW5CLENBQWdDLFlBQWhDLEVBQStDdkIsSUFBL0MsQ0FBcUQsbUJBQXJELEVBQTJFRyxJQUEzRSxDQUFpRixlQUFqRixFQUFrRyxLQUFsRzs7QUFFQTtBQUNBaEIsTUFBRyxJQUFILEVBQVVvQyxXQUFWLENBQXVCLGFBQXZCLEVBQXVDbkIsUUFBdkMsQ0FBaUQsY0FBakQ7QUFDQTtBQUVELEdBWkQ7QUFhQSxFQWREOztBQWdCQTtBQUNBaEIsS0FBSXVGLFlBQUosR0FBbUIsWUFBVztBQUM3QnZGLE1BQUlLLEVBQUosQ0FBT2dGLGlCQUFQLENBQXlCWSxPQUF6QixDQUFrQywwSUFBbEM7QUFDQSxFQUZEOztBQUlBO0FBQ0FqRyxLQUFJd0YsYUFBSixHQUFvQixVQUFVRyxDQUFWLEVBQWM7O0FBRWpDLE1BQUlPLEtBQUtuRyxFQUFHLElBQUgsQ0FBVDtBQUFBLE1BQW9CO0FBQ25Cb0csWUFBVUQsR0FBR0UsUUFBSCxDQUFhLGFBQWIsQ0FEWDtBQUFBLE1BQ3lDO0FBQ3hDUixZQUFVN0YsRUFBRzRGLEVBQUVFLE1BQUwsQ0FGWCxDQUZpQyxDQUlQOztBQUUxQjtBQUNBO0FBQ0EsTUFBS0QsUUFBUTVELFFBQVIsQ0FBa0IsWUFBbEIsS0FBb0M0RCxRQUFRNUQsUUFBUixDQUFrQixrQkFBbEIsQ0FBekMsRUFBa0Y7O0FBRWpGO0FBQ0FoQyxPQUFJK0YsZ0JBQUo7O0FBRUEsT0FBSyxDQUFFSSxRQUFRbkUsUUFBUixDQUFrQixZQUFsQixDQUFQLEVBQTBDOztBQUV6QztBQUNBaEMsUUFBSXFHLFdBQUosQ0FBaUJILEVBQWpCLEVBQXFCQyxPQUFyQjtBQUVBOztBQUVELFVBQU8sS0FBUDtBQUNBO0FBRUQsRUF2QkQ7O0FBeUJBO0FBQ0FuRyxLQUFJcUcsV0FBSixHQUFrQixVQUFVTCxNQUFWLEVBQWtCRyxPQUFsQixFQUE0Qjs7QUFFN0M7QUFDQUgsU0FBT2hGLFFBQVAsQ0FBaUIsWUFBakIsRUFBZ0NKLElBQWhDLENBQXNDLG1CQUF0QyxFQUE0REcsSUFBNUQsQ0FBa0UsZUFBbEUsRUFBbUYsSUFBbkY7O0FBRUE7QUFDQW9GLFVBQVFuRixRQUFSLENBQWtCLGlDQUFsQjtBQUVBLEVBUkQ7O0FBVUE7QUFDQWhCLEtBQUkwRixrQkFBSixHQUF5QixZQUFXOztBQUVuQztBQUNBLE1BQUssQ0FBRTNGLEVBQUcsSUFBSCxFQUFVaUMsUUFBVixDQUFvQixZQUFwQixDQUFQLEVBQTRDO0FBQzNDaEMsT0FBSUssRUFBSixDQUFPZ0YsaUJBQVAsQ0FBeUJsRCxXQUF6QixDQUFzQyxZQUF0QyxFQUFxRHZCLElBQXJELENBQTJELG1CQUEzRCxFQUFpRkcsSUFBakYsQ0FBdUYsZUFBdkYsRUFBd0csS0FBeEc7QUFDQWYsT0FBSUssRUFBSixDQUFPK0UsZ0JBQVAsQ0FBd0JqRCxXQUF4QixDQUFxQyx3QkFBckM7QUFDQTtBQUVELEVBUkQ7O0FBVUE7QUFDQXBDLEdBQUdDLElBQUlDLElBQVA7QUFFQSxDQXZIQyxFQXVIQ0osTUF2SEQsRUF1SFMrQyxNQXZIVCxFQXVIaUIvQyxPQUFPc0YsYUF2SHhCLENBQUY7OztBQ05BOzs7OztBQUtBdEYsT0FBT3lHLFFBQVAsR0FBa0IsRUFBbEI7QUFDRSxXQUFVekcsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTJCOztBQUU1QixLQUFJdUcscUJBQUo7QUFBQSxLQUNDQywyQkFERDtBQUFBLEtBRUNDLGdCQUZEO0FBQUEsS0FHQ0MsT0FBTzdELFNBQVM4RCxhQUFULENBQXdCLFFBQXhCLENBSFI7QUFBQSxLQUlDQyxrQkFBa0IvRCxTQUFTZ0Usb0JBQVQsQ0FBK0IsUUFBL0IsRUFBMEMsQ0FBMUMsQ0FKbkI7QUFBQSxLQUtDQyxXQUxEOztBQU9BO0FBQ0E5RyxLQUFJQyxJQUFKLEdBQVcsWUFBVztBQUNyQkQsTUFBSUUsS0FBSjs7QUFFQSxNQUFLRixJQUFJRyxpQkFBSixFQUFMLEVBQStCO0FBQzlCeUcsbUJBQWdCRyxVQUFoQixDQUEyQkMsWUFBM0IsQ0FBeUNOLElBQXpDLEVBQStDRSxlQUEvQztBQUNBNUcsT0FBSUksVUFBSjtBQUNBO0FBQ0QsRUFQRDs7QUFTQTtBQUNBSixLQUFJRSxLQUFKLEdBQVksWUFBVztBQUN0QkYsTUFBSUssRUFBSixHQUFTO0FBQ1IsV0FBUU4sRUFBRyxNQUFIO0FBREEsR0FBVDtBQUdBLEVBSkQ7O0FBTUE7QUFDQUMsS0FBSUcsaUJBQUosR0FBd0IsWUFBVztBQUNsQyxTQUFPSixFQUFHLGdCQUFILEVBQXNCVyxNQUE3QjtBQUNBLEVBRkQ7O0FBSUE7QUFDQVYsS0FBSUksVUFBSixHQUFpQixZQUFXOztBQUUzQjtBQUNBSixNQUFJSyxFQUFKLENBQU95QyxJQUFQLENBQVl2QyxFQUFaLENBQWdCLGtCQUFoQixFQUFvQyxnQkFBcEMsRUFBc0RQLElBQUlpSCxTQUExRDs7QUFFQTtBQUNBakgsTUFBSUssRUFBSixDQUFPeUMsSUFBUCxDQUFZdkMsRUFBWixDQUFnQixrQkFBaEIsRUFBb0MsUUFBcEMsRUFBOENQLElBQUlrSCxVQUFsRDs7QUFFQTtBQUNBbEgsTUFBSUssRUFBSixDQUFPeUMsSUFBUCxDQUFZdkMsRUFBWixDQUFnQixTQUFoQixFQUEyQlAsSUFBSW1ILFdBQS9COztBQUVBO0FBQ0FuSCxNQUFJSyxFQUFKLENBQU95QyxJQUFQLENBQVl2QyxFQUFaLENBQWdCLGtCQUFoQixFQUFvQyxnQkFBcEMsRUFBc0RQLElBQUlvSCxpQkFBMUQ7O0FBRUE7QUFDQXBILE1BQUlLLEVBQUosQ0FBT3lDLElBQVAsQ0FBWXZDLEVBQVosQ0FBZ0IsU0FBaEIsRUFBMkJQLElBQUlxSCxpQkFBL0I7QUFFQSxFQWpCRDs7QUFtQkE7QUFDQXJILEtBQUlpSCxTQUFKLEdBQWdCLFlBQVc7O0FBRTFCO0FBQ0FWLGlCQUFleEcsRUFBRyxJQUFILENBQWY7O0FBRUE7QUFDQSxNQUFJdUgsU0FBU3ZILEVBQUdBLEVBQUcsSUFBSCxFQUFVd0gsSUFBVixDQUFnQixRQUFoQixDQUFILENBQWI7O0FBRUE7QUFDQUQsU0FBT3RHLFFBQVAsQ0FBaUIsWUFBakI7O0FBRUE7QUFDQWhCLE1BQUlLLEVBQUosQ0FBT3lDLElBQVAsQ0FBWTlCLFFBQVosQ0FBc0IsWUFBdEI7O0FBRUE7QUFDQTtBQUNBO0FBQ0F3Rix1QkFBcUJjLE9BQU8xRyxJQUFQLENBQWEsdUJBQWIsQ0FBckI7O0FBRUE7QUFDQSxNQUFLLElBQUk0RixtQkFBbUI5RixNQUE1QixFQUFxQzs7QUFFcEM7QUFDQThGLHNCQUFtQixDQUFuQixFQUFzQmdCLEtBQXRCO0FBQ0E7QUFFRCxFQTFCRDs7QUE0QkE7QUFDQXhILEtBQUlrSCxVQUFKLEdBQWlCLFlBQVc7O0FBRTNCO0FBQ0EsTUFBSUksU0FBU3ZILEVBQUdBLEVBQUcsdUJBQUgsRUFBNkJ3SCxJQUE3QixDQUFtQyxRQUFuQyxDQUFILENBQWI7OztBQUVDO0FBQ0FFLFlBQVVILE9BQU8xRyxJQUFQLENBQWEsUUFBYixDQUhYOztBQUtBO0FBQ0EsTUFBSzZHLFFBQVEvRyxNQUFiLEVBQXNCOztBQUVyQjtBQUNBLE9BQUlnSCxNQUFNRCxRQUFRMUcsSUFBUixDQUFjLEtBQWQsQ0FBVjs7QUFFQTtBQUNBO0FBQ0EsT0FBSyxDQUFFMkcsSUFBSUMsUUFBSixDQUFjLGVBQWQsQ0FBUCxFQUF5Qzs7QUFFeEM7QUFDQUYsWUFBUTFHLElBQVIsQ0FBYyxLQUFkLEVBQXFCLEVBQXJCLEVBQTBCQSxJQUExQixDQUFnQyxLQUFoQyxFQUF1QzJHLEdBQXZDO0FBQ0EsSUFKRCxNQUlPOztBQUVOO0FBQ0FqQixZQUFRbUIsU0FBUjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQU4sU0FBT25GLFdBQVAsQ0FBb0IsWUFBcEI7O0FBRUE7QUFDQW5DLE1BQUlLLEVBQUosQ0FBT3lDLElBQVAsQ0FBWVgsV0FBWixDQUF5QixZQUF6Qjs7QUFFQTtBQUNBb0UsZUFBYWlCLEtBQWI7QUFFQSxFQXBDRDs7QUFzQ0E7QUFDQXhILEtBQUltSCxXQUFKLEdBQWtCLFVBQVVqRyxLQUFWLEVBQWtCO0FBQ25DLE1BQUssT0FBT0EsTUFBTTJHLE9BQWxCLEVBQTRCO0FBQzNCN0gsT0FBSWtILFVBQUo7QUFDQTtBQUNELEVBSkQ7O0FBTUE7QUFDQWxILEtBQUlvSCxpQkFBSixHQUF3QixVQUFVbEcsS0FBVixFQUFrQjs7QUFFekM7QUFDQSxNQUFLLENBQUVuQixFQUFHbUIsTUFBTTJFLE1BQVQsRUFBa0JpQyxPQUFsQixDQUEyQixLQUEzQixFQUFtQzlGLFFBQW5DLENBQTZDLGNBQTdDLENBQVAsRUFBdUU7QUFDdEVoQyxPQUFJa0gsVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBbEgsS0FBSXFILGlCQUFKLEdBQXdCLFVBQVVuRyxLQUFWLEVBQWtCOztBQUV6QztBQUNBLE1BQUssTUFBTUEsTUFBTTZHLEtBQVosSUFBcUIsSUFBSWhJLEVBQUcsYUFBSCxFQUFtQlcsTUFBakQsRUFBMEQ7QUFDekQsT0FBSXNILFdBQVdqSSxFQUFHLFFBQUgsQ0FBZjtBQUFBLE9BQ0NrSSxhQUFhekIsbUJBQW1CM0UsS0FBbkIsQ0FBMEJtRyxRQUExQixDQURkOztBQUdBLE9BQUssTUFBTUMsVUFBTixJQUFvQi9HLE1BQU1nSCxRQUEvQixFQUEwQzs7QUFFekM7QUFDQTFCLHVCQUFvQkEsbUJBQW1COUYsTUFBbkIsR0FBNEIsQ0FBaEQsRUFBb0Q4RyxLQUFwRDtBQUNBdEcsVUFBTWlILGNBQU47QUFDQSxJQUxELE1BS08sSUFBSyxDQUFFakgsTUFBTWdILFFBQVIsSUFBb0JELGVBQWV6QixtQkFBbUI5RixNQUFuQixHQUE0QixDQUFwRSxFQUF3RTs7QUFFOUU7QUFDQThGLHVCQUFtQixDQUFuQixFQUFzQmdCLEtBQXRCO0FBQ0F0RyxVQUFNaUgsY0FBTjtBQUNBO0FBQ0Q7QUFDRCxFQW5CRDs7QUFxQkE7QUFDQW5JLEtBQUlvSSx1QkFBSixHQUE4QixZQUFXO0FBQ3hDLE1BQUlkLFNBQVN2SCxFQUFHLFdBQUgsQ0FBYjtBQUFBLE1BQ0NzSSxZQUFZZixPQUFPMUcsSUFBUCxDQUFhLFFBQWIsRUFBd0JHLElBQXhCLENBQThCLElBQTlCLENBRGI7O0FBR0EwRixZQUFVLElBQUlLLEdBQUd3QixNQUFQLENBQWVELFNBQWYsRUFBMEI7QUFDbkNFLFdBQVE7QUFDUCxlQUFXdkksSUFBSXdJLGFBRFI7QUFFUCxxQkFBaUJ4SSxJQUFJeUk7QUFGZDtBQUQyQixHQUExQixDQUFWO0FBTUEsRUFWRDs7QUFZQTtBQUNBekksS0FBSXdJLGFBQUosR0FBb0IsWUFBVyxDQUM5QixDQUREOztBQUdBO0FBQ0F4SSxLQUFJeUksbUJBQUosR0FBMEIsWUFBVzs7QUFFcEM7QUFDQTFJLElBQUdtQixNQUFNMkUsTUFBTixDQUFhNkMsQ0FBaEIsRUFBb0JaLE9BQXBCLENBQTZCLFFBQTdCLEVBQXdDbEgsSUFBeEMsQ0FBOEMsdUJBQTlDLEVBQXdFK0gsS0FBeEUsR0FBZ0ZuQixLQUFoRjtBQUNBLEVBSkQ7O0FBT0E7QUFDQXpILEdBQUdDLElBQUlDLElBQVA7QUFDQSxDQXhMQyxFQXdMQ0osTUF4TEQsRUF3TFMrQyxNQXhMVCxFQXdMaUIvQyxPQUFPeUcsUUF4THhCLENBQUY7OztBQ05BOzs7OztBQUtBekcsT0FBTytJLG9CQUFQLEdBQThCLEVBQTlCO0FBQ0UsV0FBVS9JLE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEyQjs7QUFFNUI7QUFDQUEsS0FBSUMsSUFBSixHQUFXLFlBQVc7QUFDckJELE1BQUlFLEtBQUo7O0FBRUEsTUFBS0YsSUFBSUcsaUJBQUosRUFBTCxFQUErQjtBQUM5QkgsT0FBSUksVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBSixLQUFJRSxLQUFKLEdBQVksWUFBVztBQUN0QkYsTUFBSUssRUFBSixHQUFTO0FBQ1JSLFdBQVFFLEVBQUdGLE1BQUgsQ0FEQTtBQUVSdUYscUJBQWtCckYsRUFBRyw0QkFBSCxDQUZWO0FBR1JzRixzQkFBbUJ0RixFQUFHLDRDQUFIO0FBSFgsR0FBVDtBQUtBLEVBTkQ7O0FBUUE7QUFDQUMsS0FBSUksVUFBSixHQUFpQixZQUFXO0FBQzNCSixNQUFJSyxFQUFKLENBQU9SLE1BQVAsQ0FBY1UsRUFBZCxDQUFrQixNQUFsQixFQUEwQlAsSUFBSXVGLFlBQTlCO0FBQ0F2RixNQUFJSyxFQUFKLENBQU9nRixpQkFBUCxDQUF5QnpFLElBQXpCLENBQStCLEdBQS9CLEVBQXFDTCxFQUFyQyxDQUF5QyxrQkFBekMsRUFBNkRQLElBQUk2SSxXQUFqRTtBQUNBLEVBSEQ7O0FBS0E7QUFDQTdJLEtBQUlHLGlCQUFKLEdBQXdCLFlBQVc7QUFDbEMsU0FBT0gsSUFBSUssRUFBSixDQUFPK0UsZ0JBQVAsQ0FBd0IxRSxNQUEvQjtBQUNBLEVBRkQ7O0FBSUE7QUFDQVYsS0FBSXVGLFlBQUosR0FBbUIsWUFBVztBQUM3QnZGLE1BQUlLLEVBQUosQ0FBT2dGLGlCQUFQLENBQXlCekUsSUFBekIsQ0FBK0IsS0FBL0IsRUFBdUNrSSxNQUF2QyxDQUErQyxxREFBL0M7QUFDQSxFQUZEOztBQUlBO0FBQ0E5SSxLQUFJNkksV0FBSixHQUFrQixZQUFXO0FBQzVCOUksSUFBRyxJQUFILEVBQVUrSCxPQUFWLENBQW1CLDJCQUFuQixFQUFpRDdDLFdBQWpELENBQThELE9BQTlEO0FBQ0EsRUFGRDs7QUFJQTtBQUNBbEYsR0FBR0MsSUFBSUMsSUFBUDtBQUVBLENBNUNDLEVBNENDSixNQTVDRCxFQTRDUytDLE1BNUNULEVBNENpQi9DLE9BQU8rSSxvQkE1Q3hCLENBQUY7OztBQ05BOzs7OztBQUtBL0ksT0FBT2tKLFlBQVAsR0FBc0IsRUFBdEI7QUFDRSxXQUFVbEosTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTJCOztBQUU1QjtBQUNBQSxLQUFJQyxJQUFKLEdBQVcsWUFBVztBQUNyQkQsTUFBSUUsS0FBSjs7QUFFQSxNQUFLRixJQUFJRyxpQkFBSixFQUFMLEVBQStCO0FBQzlCSCxPQUFJSSxVQUFKO0FBQ0E7QUFDRCxFQU5EOztBQVFBO0FBQ0FKLEtBQUlFLEtBQUosR0FBWSxZQUFXO0FBQ3RCRixNQUFJSyxFQUFKLEdBQVM7QUFDUnlDLFNBQU0vQyxFQUFHLE1BQUgsQ0FERTtBQUVSaUosbUJBQWdCakosRUFBRyxtQkFBSCxDQUZSO0FBR1J1Rix1QkFBb0J2RixFQUFHLHVCQUFILENBSFo7QUFJUmtKLGtCQUFlbEosRUFBRyxrQkFBSCxDQUpQO0FBS1JtSixvQkFBaUJuSixFQUFHLG9CQUFIO0FBTFQsR0FBVDtBQU9BLEVBUkQ7O0FBVUE7QUFDQUMsS0FBSUksVUFBSixHQUFpQixZQUFXO0FBQzNCSixNQUFJSyxFQUFKLENBQU95QyxJQUFQLENBQVl2QyxFQUFaLENBQWdCLFNBQWhCLEVBQTJCUCxJQUFJbUgsV0FBL0I7QUFDQW5ILE1BQUlLLEVBQUosQ0FBTzJJLGNBQVAsQ0FBc0J6SSxFQUF0QixDQUEwQixPQUExQixFQUFtQ1AsSUFBSW1KLGNBQXZDO0FBQ0FuSixNQUFJSyxFQUFKLENBQU80SSxhQUFQLENBQXFCMUksRUFBckIsQ0FBeUIsT0FBekIsRUFBa0NQLElBQUlvSixlQUF0QztBQUNBcEosTUFBSUssRUFBSixDQUFPNkksZUFBUCxDQUF1QjNJLEVBQXZCLENBQTJCLE9BQTNCLEVBQW9DUCxJQUFJbUosY0FBeEM7QUFDQSxFQUxEOztBQU9BO0FBQ0FuSixLQUFJRyxpQkFBSixHQUF3QixZQUFXO0FBQ2xDLFNBQU9ILElBQUlLLEVBQUosQ0FBT2lGLGtCQUFQLENBQTBCNUUsTUFBakM7QUFDQSxFQUZEOztBQUlBO0FBQ0FWLEtBQUlvSixlQUFKLEdBQXNCLFlBQVc7O0FBRWhDLE1BQUssV0FBV3JKLEVBQUcsSUFBSCxFQUFVZ0IsSUFBVixDQUFnQixlQUFoQixDQUFoQixFQUFvRDtBQUNuRGYsT0FBSW1KLGNBQUo7QUFDQSxHQUZELE1BRU87QUFDTm5KLE9BQUlxSixhQUFKO0FBQ0E7QUFFRCxFQVJEOztBQVVBO0FBQ0FySixLQUFJcUosYUFBSixHQUFvQixZQUFXO0FBQzlCckosTUFBSUssRUFBSixDQUFPaUYsa0JBQVAsQ0FBMEJ0RSxRQUExQixDQUFvQyxZQUFwQztBQUNBaEIsTUFBSUssRUFBSixDQUFPNEksYUFBUCxDQUFxQmpJLFFBQXJCLENBQStCLFlBQS9CO0FBQ0FoQixNQUFJSyxFQUFKLENBQU82SSxlQUFQLENBQXVCbEksUUFBdkIsQ0FBaUMsWUFBakM7O0FBRUFoQixNQUFJSyxFQUFKLENBQU80SSxhQUFQLENBQXFCbEksSUFBckIsQ0FBMkIsZUFBM0IsRUFBNEMsSUFBNUM7QUFDQWYsTUFBSUssRUFBSixDQUFPaUYsa0JBQVAsQ0FBMEJ2RSxJQUExQixDQUFnQyxhQUFoQyxFQUErQyxLQUEvQzs7QUFFQWYsTUFBSUssRUFBSixDQUFPaUYsa0JBQVAsQ0FBMEIxRSxJQUExQixDQUFnQyxRQUFoQyxFQUEyQytILEtBQTNDLEdBQW1EbkIsS0FBbkQ7QUFDQSxFQVREOztBQVdBO0FBQ0F4SCxLQUFJbUosY0FBSixHQUFxQixZQUFXO0FBQy9CbkosTUFBSUssRUFBSixDQUFPaUYsa0JBQVAsQ0FBMEJuRCxXQUExQixDQUF1QyxZQUF2QztBQUNBbkMsTUFBSUssRUFBSixDQUFPNEksYUFBUCxDQUFxQjlHLFdBQXJCLENBQWtDLFlBQWxDO0FBQ0FuQyxNQUFJSyxFQUFKLENBQU82SSxlQUFQLENBQXVCL0csV0FBdkIsQ0FBb0MsWUFBcEM7O0FBRUFuQyxNQUFJSyxFQUFKLENBQU80SSxhQUFQLENBQXFCbEksSUFBckIsQ0FBMkIsZUFBM0IsRUFBNEMsS0FBNUM7QUFDQWYsTUFBSUssRUFBSixDQUFPaUYsa0JBQVAsQ0FBMEJ2RSxJQUExQixDQUFnQyxhQUFoQyxFQUErQyxJQUEvQzs7QUFFQWYsTUFBSUssRUFBSixDQUFPNEksYUFBUCxDQUFxQnpCLEtBQXJCO0FBQ0EsRUFURDs7QUFXQTtBQUNBeEgsS0FBSW1ILFdBQUosR0FBa0IsVUFBVWpHLEtBQVYsRUFBa0I7QUFDbkMsTUFBSyxPQUFPQSxNQUFNMkcsT0FBbEIsRUFBNEI7QUFDM0I3SCxPQUFJbUosY0FBSjtBQUNBO0FBQ0QsRUFKRDs7QUFNQTtBQUNBcEosR0FBR0MsSUFBSUMsSUFBUDtBQUVBLENBaEZDLEVBZ0ZDSixNQWhGRCxFQWdGUytDLE1BaEZULEVBZ0ZpQi9DLE9BQU9rSixZQWhGeEIsQ0FBRjs7O0FDTkE7Ozs7Ozs7QUFPRSxhQUFXO0FBQ1osS0FBSU8sV0FBVyxDQUFDLENBQUQsR0FBS0MsVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTJDLFFBQTNDLENBQXBCO0FBQUEsS0FDQ0MsVUFBVSxDQUFDLENBQUQsR0FBS0osVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTJDLE9BQTNDLENBRGhCO0FBQUEsS0FFQ0UsT0FBTyxDQUFDLENBQUQsR0FBS0wsVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTJDLE1BQTNDLENBRmI7O0FBSUEsS0FBSyxDQUFFSixZQUFZSyxPQUFaLElBQXVCQyxJQUF6QixLQUFtQy9HLFNBQVNnSCxjQUE1QyxJQUE4RGhLLE9BQU9pSyxnQkFBMUUsRUFBNkY7QUFDNUZqSyxTQUFPaUssZ0JBQVAsQ0FBeUIsWUFBekIsRUFBdUMsWUFBVztBQUNqRCxPQUFJQyxLQUFLQyxTQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBeUIsQ0FBekIsQ0FBVDtBQUFBLE9BQ0NwSSxPQUREOztBQUdBLE9BQUssQ0FBSSxlQUFGLENBQW9CcUksSUFBcEIsQ0FBMEJKLEVBQTFCLENBQVAsRUFBd0M7QUFDdkM7QUFDQTs7QUFFRGpJLGFBQVVlLFNBQVNnSCxjQUFULENBQXlCRSxFQUF6QixDQUFWOztBQUVBLE9BQUtqSSxPQUFMLEVBQWU7QUFDZCxRQUFLLENBQUksdUNBQUYsQ0FBNENxSSxJQUE1QyxDQUFrRHJJLFFBQVFzSSxPQUExRCxDQUFQLEVBQTZFO0FBQzVFdEksYUFBUXVJLFFBQVIsR0FBbUIsQ0FBQyxDQUFwQjtBQUNBOztBQUVEdkksWUFBUTBGLEtBQVI7QUFDQTtBQUNELEdBakJELEVBaUJHLEtBakJIO0FBa0JBO0FBQ0QsQ0F6QkMsR0FBRjs7O0FDUEE7Ozs7O0FBS0EzSCxPQUFPeUssY0FBUCxHQUF3QixFQUF4QjtBQUNFLFdBQVV6SyxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMkI7O0FBRTVCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFXO0FBQ3JCRCxNQUFJRSxLQUFKO0FBQ0FGLE1BQUlJLFVBQUo7QUFDQSxFQUhEOztBQUtBO0FBQ0FKLEtBQUlFLEtBQUosR0FBWSxZQUFXO0FBQ3RCRixNQUFJSyxFQUFKLEdBQVM7QUFDUixhQUFVTixFQUFHRixNQUFILENBREY7QUFFUixXQUFRRSxFQUFHOEMsU0FBU0MsSUFBWjtBQUZBLEdBQVQ7QUFJQSxFQUxEOztBQU9BO0FBQ0E5QyxLQUFJSSxVQUFKLEdBQWlCLFlBQVc7QUFDM0JKLE1BQUlLLEVBQUosQ0FBT1IsTUFBUCxDQUFjMEssSUFBZCxDQUFvQnZLLElBQUl3SyxZQUF4QjtBQUNBLEVBRkQ7O0FBSUE7QUFDQXhLLEtBQUl3SyxZQUFKLEdBQW1CLFlBQVc7QUFDN0J4SyxNQUFJSyxFQUFKLENBQU95QyxJQUFQLENBQVk5QixRQUFaLENBQXNCLE9BQXRCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBakIsR0FBR0MsSUFBSUMsSUFBUDtBQUNBLENBNUJDLEVBNEJDSixNQTVCRCxFQTRCUytDLE1BNUJULEVBNEJpQi9DLE9BQU95SyxjQTVCeEIsQ0FBRiIsImZpbGUiOiJwcm9qZWN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBGaWxlIGhlcm8tY2Fyb3VzZWwuanNcbiAqXG4gKiBDcmVhdGUgYSBjYXJvdXNlbCBpZiB3ZSBoYXZlIG1vcmUgdGhhbiBvbmUgaGVybyBzbGlkZS5cbiAqL1xud2luZG93Lndkc0hlcm9DYXJvdXNlbCA9IHt9O1xuKCBmdW5jdGlvbiggd2luZG93LCAkLCBhcHAgKSB7XG5cblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLmNhY2hlKCk7XG5cblx0XHRpZiAoIGFwcC5tZWV0c1JlcXVpcmVtZW50cygpICkge1xuXHRcdFx0YXBwLmJpbmRFdmVudHMoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gQ2FjaGUgYWxsIHRoZSB0aGluZ3MuXG5cdGFwcC5jYWNoZSA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYyA9IHtcblx0XHRcdHdpbmRvdzogJCggd2luZG93ICksXG5cdFx0XHRoZXJvQ2Fyb3VzZWw6ICQoICcuY2Fyb3VzZWwnIClcblx0XHR9O1xuXHR9O1xuXG5cdC8vIENvbWJpbmUgYWxsIGV2ZW50cy5cblx0YXBwLmJpbmRFdmVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMud2luZG93Lm9uKCAnbG9hZCcsIGFwcC5kb1NsaWNrICk7XG5cdFx0YXBwLiRjLndpbmRvdy5vbiggJ2xvYWQnLCBhcHAuZG9GaXJzdEFuaW1hdGlvbiApO1xuXG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gYXBwLiRjLmhlcm9DYXJvdXNlbC5sZW5ndGg7XG5cdH07XG5cblx0Ly8gQW5pbWF0ZSB0aGUgZmlyc3Qgc2xpZGUgb24gd2luZG93IGxvYWQuXG5cdGFwcC5kb0ZpcnN0QW5pbWF0aW9uID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBHZXQgdGhlIGZpcnN0IHNsaWRlIGNvbnRlbnQgYXJlYSBhbmQgYW5pbWF0aW9uIGF0dHJpYnV0ZS5cblx0XHRsZXQgZmlyc3RTbGlkZSA9IGFwcC4kYy5oZXJvQ2Fyb3VzZWwuZmluZCggJ1tkYXRhLXNsaWNrLWluZGV4PTBdJyApLFxuXHRcdFx0Zmlyc3RTbGlkZUNvbnRlbnQgPSBmaXJzdFNsaWRlLmZpbmQoICcuaGVyby1jb250ZW50JyApLFxuXHRcdFx0Zmlyc3RBbmltYXRpb24gPSBmaXJzdFNsaWRlQ29udGVudC5hdHRyKCAnZGF0YS1hbmltYXRpb24nICk7XG5cblx0XHQvLyBBZGQgdGhlIGFuaW1hdGlvbiBjbGFzcyB0byB0aGUgZmlyc3Qgc2xpZGUuXG5cdFx0Zmlyc3RTbGlkZUNvbnRlbnQuYWRkQ2xhc3MoIGZpcnN0QW5pbWF0aW9uICk7XG5cdH07XG5cblx0Ly8gQW5pbWF0ZSB0aGUgc2xpZGUgY29udGVudC5cblx0YXBwLmRvQW5pbWF0aW9uID0gZnVuY3Rpb24oIGV2ZW50LCBzbGljayApIHtcblxuXHRcdGxldCBzbGlkZXMgPSAkKCAnLnNsaWRlJyApLFxuXHRcdFx0YWN0aXZlU2xpZGUgPSAkKCAnLnNsaWNrLWN1cnJlbnQnICksXG5cdFx0XHRhY3RpdmVDb250ZW50ID0gYWN0aXZlU2xpZGUuZmluZCggJy5oZXJvLWNvbnRlbnQnICksXG5cblx0XHRcdC8vIFRoaXMgaXMgYSBzdHJpbmcgbGlrZSBzbzogJ2FuaW1hdGVkIHNvbWVDc3NDbGFzcycuXG5cdFx0XHRhbmltYXRpb25DbGFzcyA9IGFjdGl2ZUNvbnRlbnQuYXR0ciggJ2RhdGEtYW5pbWF0aW9uJyApLFxuXHRcdFx0c3BsaXRBbmltYXRpb24gPSBhbmltYXRpb25DbGFzcy5zcGxpdCggJyAnICksXG5cblx0XHRcdC8vIFRoaXMgaXMgdGhlICdhbmltYXRlZCcgY2xhc3MuXG5cdFx0XHRhbmltYXRpb25UcmlnZ2VyID0gc3BsaXRBbmltYXRpb25bMF0sXG5cblx0XHQvLyBUaGlzIGlzIHRoZSBhbmltYXRlLmNzcyBjbGFzcy5cblx0XHRhbmltYXRlQ3NzID0gc3BsaXRBbmltYXRpb25bMV07XG5cblx0XHQvLyBHbyB0aHJvdWdoIGVhY2ggc2xpZGUgdG8gc2VlIGlmIHdlJ3ZlIGFscmVhZHkgc2V0IGFuaW1hdGlvbiBjbGFzc2VzLlxuXHRcdHNsaWRlcy5lYWNoKCBmdW5jdGlvbiggaW5kZXgsIGVsZW1lbnQgKSB7XG5cblx0XHRcdGxldCBzbGlkZUNvbnRlbnQgPSAkKCB0aGlzICkuZmluZCggJy5oZXJvLWNvbnRlbnQnICk7XG5cblx0XHRcdC8vIElmIHdlJ3ZlIHNldCBhbmltYXRpb24gY2xhc3NlcyBvbiBhIHNsaWRlLCByZW1vdmUgdGhlbS5cblx0XHRcdGlmICggc2xpZGVDb250ZW50Lmhhc0NsYXNzKCAnYW5pbWF0ZWQnICkgKSB7XG5cblx0XHRcdFx0Ly8gR2V0IHRoZSBsYXN0IGNsYXNzLCB3aGljaCBpcyB0aGUgYW5pbWF0ZS5jc3MgY2xhc3MuXG5cdFx0XHRcdGxldCBsYXN0Q2xhc3MgPSBzbGlkZUNvbnRlbnQuYXR0ciggJ2NsYXNzJyApLnNwbGl0KCAnICcgKS5wb3AoICk7XG5cblx0XHRcdFx0Ly8gUmVtb3ZlIGJvdGggYW5pbWF0aW9uIGNsYXNzZXMuXG5cdFx0XHRcdHNsaWRlQ29udGVudC5yZW1vdmVDbGFzcyggbGFzdENsYXNzICkucmVtb3ZlQ2xhc3MoIGFuaW1hdGlvblRyaWdnZXIgKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXHRcdC8vIEFkZCBhbmltYXRpb24gY2xhc3NlcyBhZnRlciBzbGlkZSBpcyBpbiB2aWV3LlxuXHRcdGFjdGl2ZUNvbnRlbnQuYWRkQ2xhc3MoIGFuaW1hdGlvbkNsYXNzICk7XG5cdH07XG5cblx0Ly8gQWxsb3cgYmFja2dyb3VuZCB2aWRlb3MgdG8gYXV0b3BsYXkuXG5cdGFwcC5wbGF5QmFja2dyb3VuZFZpZGVvcyA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0Ly8gR2V0IGFsbCB0aGUgdmlkZW9zIGluIG91ciBzbGlkZXMgb2JqZWN0LlxuXHRcdCQoICd2aWRlbycgKS5lYWNoKCBmdW5jdGlvbigpIHtcblxuXHRcdFx0Ly8gTGV0IHRoZW0gYXV0b3BsYXkuIFRPRE86IFBvc3NpYmx5IGNoYW5nZSB0aGlzIGxhdGVyIHRvIG9ubHkgcGxheSB0aGUgdmlzaWJsZSBzbGlkZSB2aWRlby5cblx0XHRcdHRoaXMucGxheSgpO1xuXHRcdH0pO1xuXHR9O1xuXG5cdC8vIEtpY2sgb2ZmIFNsaWNrLlxuXHRhcHAuZG9TbGljayA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0YXBwLiRjLmhlcm9DYXJvdXNlbC5vbiggJ2luaXQnLCBhcHAucGxheUJhY2tncm91bmRWaWRlb3MgKTtcblxuXHRcdGFwcC4kYy5oZXJvQ2Fyb3VzZWwuc2xpY2soe1xuXHRcdFx0YXV0b3BsYXk6IHRydWUsXG5cdFx0XHRhdXRvcGxheVNwZWVkOiA1MDAwLFxuXHRcdFx0YXJyb3dzOiBmYWxzZSxcblx0XHRcdGRvdHM6IGZhbHNlLFxuXHRcdFx0Zm9jdXNPblNlbGVjdDogdHJ1ZSxcblx0XHRcdHdhaXRGb3JBbmltYXRlOiB0cnVlXG5cdFx0fSk7XG5cblx0XHRhcHAuJGMuaGVyb0Nhcm91c2VsLm9uKCAnYWZ0ZXJDaGFuZ2UnLCBhcHAuZG9BbmltYXRpb24gKTtcblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG5cbn0oIHdpbmRvdywgalF1ZXJ5LCB3aW5kb3cud2RzSGVyb0Nhcm91c2VsICkgKTtcbiIsIi8qKlxuICogRmlsZSBqcy1lbmFibGVkLmpzXG4gKlxuICogSWYgSmF2YXNjcmlwdCBpcyBlbmFibGVkLCByZXBsYWNlIHRoZSA8Ym9keT4gY2xhc3MgXCJuby1qc1wiLlxuICovXG5kb2N1bWVudC5ib2R5LmNsYXNzTmFtZSA9IGRvY3VtZW50LmJvZHkuY2xhc3NOYW1lLnJlcGxhY2UoICduby1qcycsICdqcycgKTtcbiIsIlxuLy8gKGZ1bmN0aW9uKCl7XG5cbndpbmRvdy5tZW51QW5pbWF0aW9uID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csIFR3ZWVuTWF4LCAkLCBhcHAgKSB7XG4gICAgY29uc29sZS5sb2coXCJNZW51IGFuaW1hdGlvblwiKTtcbiAgICBcbi8vIGNvbnNvbGUubG9nKFR3ZWVuTWF4KTtcbiAgICAvLyAgIGNvbnNvbGUubG9nKFR3ZWVubWF4KTtcbiAgICAgfSh3aW5kb3csIFR3ZWVuTWF4ICwgalF1ZXJ5LCB3aW5kb3cubWVudUFuaW1hdGlvbikpOyIsIlxuLy8gKGZ1bmN0aW9uKCl7XG5cbkJhcmJhLlBqYXguc3RhcnQoKTtcbndpbmRvdy5tZW51VHJpZ2dlciA9IHt9O1xuICB2YXIgbWVudVBvc2l0aW9uVG9wO1xuICB2YXIgbWVudVBvc2l0aW9uTGVmdDtcbiAgdmFyIG5hdldpZHRoO1xuKCBmdW5jdGlvbiggd2luZG93LCAkLCBUd2Vlbk1heCAsYXBwICkge1xuJChmdW5jdGlvbigpe1xuICB2YXIgbWVudUNvbXBsZXRlID0gKCkgPT4ge1xuICAgIGNvbnNvbGUubG9nKFwibWVudSBjb21wbGV0ZVwiKTtcbiAgICAgbWVudVBvc2l0aW9uVG9wID0gJChcIiNwcmltYXJ5LW1lbnVcIikub2Zmc2V0KCkudG9wO1xuICAgICBtZW51UG9zaXRpb25MZWZ0ID0gJChcIiNwcmltYXJ5LW1lbnVcIikub2Zmc2V0KCkubGVmdDtcbiAgICAgbmF2V2lkdGggPSAkKCcjc2l0ZS1uYXZpZ2F0aW9uJykud2lkdGgoKTtcbiAgICAgY29uc29sZS5sb2coXCJuYXYgd2lkdGggaXMgOiBcIiArIG5hdldpZHRoKTtcbiAgICAgICBjb25zb2xlLmxvZyhtZW51UG9zaXRpb25Ub3AgKyBcIiAuIFwiICsgbWVudVBvc2l0aW9uTGVmdCk7XG4gICAgICAvLyByZXR1cm4gbWVudVBvc2l0aW9uTGVmdDtcbiAgfTtcbiAgbWVudUNvbXBsZXRlKCk7XG5cbiAgICAgIFR3ZWVuTWF4LnNldChcInN2ZyNsb2dvdGlwby1zdmctbG9nb1wiLHtzY2FsZToxfSk7XG5cbiAgICAgIC8vIGNvbnNvbGUubG9nKG1lbnVQb3NpdGlvblRvcCArIFwiIC4gXCIgKyBtZW51UG9zaXRpb25MZWZ0KTtcbiAgICAgIHZhciBBbmltID0gVHdlZW5NYXgudG8oIFwiI3NpdGUtbmF2aWdhdGlvblwiICwgLjQgICx7IHg6XCIwJVwiICwgcmV2ZXJzZWQ6dHJ1ZSAscGF1c2VkOnRydWUsIGVhc2U6IFBvd2VyMi5lYXNlSW5PdXQsIG9uQ29tcGxldGU6IG1lbnVDb21wbGV0ZSB9KSA7XG4gICAgICB2YXIgQW5pbUFmdGVyID0gVHdlZW5NYXgudG8oIFwiI3NpdGUtbmF2aWdhdGlvbjo6YWZ0ZXJcIiAsIC40ICAseyB4OlwiMCVcIiAsIHJldmVyc2VkOnRydWUgLHBhdXNlZDp0cnVlLCBlYXNlOiBQb3dlcjIuZWFzZUluT3V0IH0sMykgO1xuICAgICAgdmFyIEFuaW1Cb3JkZXIgPSBUd2Vlbk1heC50byggXCIjYm9yZGVycyAudG9wLCAjYm9yZGVycyAuYm90dG9tXCIgLCAuNCAgLHsgaGVpZ2h0OlwiNzBweFwiICwgcmV2ZXJzZWQ6dHJ1ZSAscGF1c2VkOnRydWUsIGVhc2U6IFBvd2VyMi5lYXNlSW5PdXQgfSkgO1xuICAgICAgdmFyIEFuaW1Mb2dvID0gVHdlZW5NYXgudG8oIFwic3ZnI2xvZ290aXBvLXN2Zy1sb2dvXCIgLCAuNSAgLHsgeDotbWVudVBvc2l0aW9uTGVmdCAvIDMseTptZW51UG9zaXRpb25Ub3AgLyAyLHNjYWxlOjIgLCByZXZlcnNlZDp0cnVlICxwYXVzZWQ6dHJ1ZSwgZWFzZTogUG93ZXIyLmVhc2VJbk91dCB9KSA7XG5cbiAgICAkKCcubWVudS10cmlnZ2VyJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgIEFuaW0ucmV2ZXJzZWQoKSA/IEFuaW0ucGxheSgpIDogQW5pbS5yZXZlcnNlKCk7XG4gICAgICBBbmltQWZ0ZXIucmV2ZXJzZWQoKSA/IEFuaW1BZnRlci5wbGF5KCkgOiBBbmltQWZ0ZXIucmV2ZXJzZSgpO1xuICAgICAgQW5pbUJvcmRlci5yZXZlcnNlZCgpID8gQW5pbUJvcmRlci5wbGF5KCkgOiBBbmltQm9yZGVyLnJldmVyc2UoKTtcbiAgICAgIEFuaW1Mb2dvLnJldmVyc2VkKCkgPyBBbmltTG9nby5wbGF5KCkgOiBBbmltTG9nby5yZXZlcnNlKCk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSk7XG4gIH0pO1xuICAgIGNvbnNvbGUubG9nKFwidHJpZ2dlclwiKTtcbiAgICAgfSh3aW5kb3csIGpRdWVyeSwgVHdlZW5NYXgsIHdpbmRvdy5tZW51VHJpZ2dlcikpO1xuIiwiLyoqXG4gKiBGaWxlOiBtb2JpbGUtbWVudS5qc1xuICpcbiAqIENyZWF0ZSBhbiBhY2NvcmRpb24gc3R5bGUgZHJvcGRvd24uXG4gKi9cbndpbmRvdy53ZHNNb2JpbGVNZW51ID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKCB3aW5kb3cgKSxcblx0XHRcdHN1Yk1lbnVDb250YWluZXI6ICQoICcubW9iaWxlLW1lbnUgLnN1Yi1tZW51JyApLFxuXHRcdFx0c3ViTWVudVBhcmVudEl0ZW06ICQoICcubW9iaWxlLW1lbnUgbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlbicgKSxcblx0XHRcdG9mZkNhbnZhc0NvbnRhaW5lcjogJCggJy5vZmYtY2FudmFzLWNvbnRhaW5lcicgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy53aW5kb3cub24oICdsb2FkJywgYXBwLmFkZERvd25BcnJvdyApO1xuXHRcdGFwcC4kYy5zdWJNZW51UGFyZW50SXRlbS5vbiggJ2NsaWNrJywgYXBwLnRvZ2dsZVN1Ym1lbnUgKTtcblx0XHRhcHAuJGMuc3ViTWVudVBhcmVudEl0ZW0ub24oICd0cmFuc2l0aW9uZW5kJywgYXBwLnJlc2V0U3ViTWVudSApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNDb250YWluZXIub24oICd0cmFuc2l0aW9uZW5kJywgYXBwLmZvcmNlQ2xvc2VTdWJtZW51cyApO1xuXHR9O1xuXG5cdC8vIERvIHdlIG1lZXQgdGhlIHJlcXVpcmVtZW50cz9cblx0YXBwLm1lZXRzUmVxdWlyZW1lbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIGFwcC4kYy5zdWJNZW51Q29udGFpbmVyLmxlbmd0aDtcblx0fTtcblxuXHQvLyBSZXNldCB0aGUgc3VibWVudXMgYWZ0ZXIgaXQncyBkb25lIGNsb3NpbmcuXG5cdGFwcC5yZXNldFN1Yk1lbnUgPSBmdW5jdGlvbiggZSApIHtcblx0XHRjb25zdCAkdGFyZ2V0ID0gJCggZS50YXJnZXQgKTtcblxuXHRcdC8vIFdoZW4gdGhlIGxpc3QgaXRlbSBpcyBkb25lIHRyYW5zaXRpb25pbmcgaW4gaGVpZ2h0LFxuXHRcdC8vIHJlbW92ZSB0aGUgY2xhc3NlcyBmcm9tIHRoZSBzdWJtZW51IHNvIGl0IGlzIHJlYWR5IHRvIHRvZ2dsZSBhZ2Fpbi5cblx0XHRpZiAoICR0YXJnZXQuaXMoICdsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApICYmICEgJHRhcmdldC5oYXNDbGFzcyggJ2lzLXZpc2libGUnICkgKSB7XG5cdFx0XHQkdGFyZ2V0LmZpbmQoICd1bC5zdWItbWVudScgKS5yZW1vdmVDbGFzcyggJ3NsaWRlT3V0TGVmdCBpcy12aXNpYmxlJyApO1xuXHRcdH1cblxuXHR9O1xuXG5cdC8vIFNsaWRlIG91dCB0aGUgc3VibWVudSBpdGVtcy5cblx0YXBwLnNsaWRlT3V0U3ViTWVudXMgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMuc3ViTWVudUNvbnRhaW5lci5lYWNoKCBmdW5jdGlvbigpIHtcblxuXHRcdFx0Ly8gT25seSB0cnkgdG8gY2xvc2Ugc3VibWVudXMgdGhhdCBhcmUgYWN0dWFsbHkgb3Blbi5cblx0XHRcdGlmICggJCggdGhpcyApLmhhc0NsYXNzKCAnc2xpZGVJbkxlZnQnICkgKSB7XG5cblx0XHRcdFx0Ly8gQ2xvc2UgdGhlIHBhcmVudCBsaXN0IGl0ZW0sIGFuZCBzZXQgdGhlIGNvcnJlc3BvbmRpbmcgYnV0dG9uIGFyaWEgdG8gZmFsc2UuXG5cdFx0XHRcdCQoIHRoaXMgKS5wYXJlbnQoKS5yZW1vdmVDbGFzcyggJ2lzLXZpc2libGUnICkuZmluZCggJy5wYXJlbnQtaW5kaWNhdG9yJyApLmF0dHIoICdhcmlhLWV4cGFuZGVkJywgZmFsc2UgKTtcblxuXHRcdFx0XHQvLyBTbGlkZSBvdXQgdGhlIHN1Ym1lbnUuXG5cdFx0XHRcdCQoIHRoaXMgKS5yZW1vdmVDbGFzcyggJ3NsaWRlSW5MZWZ0JyApLmFkZENsYXNzKCAnc2xpZGVPdXRMZWZ0JyApO1xuXHRcdFx0fVxuXG5cdFx0fSk7XG5cdH07XG5cblx0Ly8gQWRkIHRoZSBkb3duIGFycm93IHRvIHN1Ym1lbnUgcGFyZW50cy5cblx0YXBwLmFkZERvd25BcnJvdyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy5zdWJNZW51UGFyZW50SXRlbS5wcmVwZW5kKCAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgYXJpYS1leHBhbmRlZD1cImZhbHNlXCIgY2xhc3M9XCJwYXJlbnQtaW5kaWNhdG9yXCIgYXJpYS1sYWJlbD1cIk9wZW4gc3VibWVudVwiPjxzcGFuIGNsYXNzPVwiZG93bi1hcnJvd1wiPjwvc3Bhbj48L2J1dHRvbj4nICk7XG5cdH07XG5cblx0Ly8gRGVhbCB3aXRoIHRoZSBzdWJtZW51LlxuXHRhcHAudG9nZ2xlU3VibWVudSA9IGZ1bmN0aW9uKCBlICkge1xuXG5cdFx0bGV0IGVsID0gJCggdGhpcyApLCAvLyBUaGUgbWVudSBlbGVtZW50IHdoaWNoIHdhcyBjbGlja2VkIG9uLlxuXHRcdFx0c3ViTWVudSA9IGVsLmNoaWxkcmVuKCAndWwuc3ViLW1lbnUnICksIC8vIFRoZSBuZWFyZXN0IHN1Ym1lbnUuXG5cdFx0XHQkdGFyZ2V0ID0gJCggZS50YXJnZXQgKTsgLy8gdGhlIGVsZW1lbnQgdGhhdCdzIGFjdHVhbGx5IGJlaW5nIGNsaWNrZWQgKGNoaWxkIG9mIHRoZSBsaSB0aGF0IHRyaWdnZXJlZCB0aGUgY2xpY2sgZXZlbnQpLlxuXG5cdFx0Ly8gRmlndXJlIG91dCBpZiB3ZSdyZSBjbGlja2luZyB0aGUgYnV0dG9uIG9yIGl0cyBhcnJvdyBjaGlsZCxcblx0XHQvLyBpZiBzbywgd2UgY2FuIGp1c3Qgb3BlbiBvciBjbG9zZSB0aGUgbWVudSBhbmQgYmFpbC5cblx0XHRpZiAoICR0YXJnZXQuaGFzQ2xhc3MoICdkb3duLWFycm93JyApIHx8ICR0YXJnZXQuaGFzQ2xhc3MoICdwYXJlbnQtaW5kaWNhdG9yJyApICkge1xuXG5cdFx0XHQvLyBGaXJzdCwgY29sbGFwc2UgYW55IGFscmVhZHkgb3BlbmVkIHN1Ym1lbnVzLlxuXHRcdFx0YXBwLnNsaWRlT3V0U3ViTWVudXMoKTtcblxuXHRcdFx0aWYgKCAhIHN1Yk1lbnUuaGFzQ2xhc3MoICdpcy12aXNpYmxlJyApICkge1xuXG5cdFx0XHRcdC8vIE9wZW4gdGhlIHN1Ym1lbnUuXG5cdFx0XHRcdGFwcC5vcGVuU3VibWVudSggZWwsIHN1Yk1lbnUgKTtcblxuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXG5cdH07XG5cblx0Ly8gT3BlbiBhIHN1Ym1lbnUuXG5cdGFwcC5vcGVuU3VibWVudSA9IGZ1bmN0aW9uKCBwYXJlbnQsIHN1Yk1lbnUgKSB7XG5cblx0XHQvLyBFeHBhbmQgdGhlIGxpc3QgbWVudSBpdGVtLCBhbmQgc2V0IHRoZSBjb3JyZXNwb25kaW5nIGJ1dHRvbiBhcmlhIHRvIHRydWUuXG5cdFx0cGFyZW50LmFkZENsYXNzKCAnaXMtdmlzaWJsZScgKS5maW5kKCAnLnBhcmVudC1pbmRpY2F0b3InICkuYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCB0cnVlICk7XG5cblx0XHQvLyBTbGlkZSB0aGUgbWVudSBpbi5cblx0XHRzdWJNZW51LmFkZENsYXNzKCAnaXMtdmlzaWJsZSBhbmltYXRlZCBzbGlkZUluTGVmdCcgKTtcblxuXHR9O1xuXG5cdC8vIEZvcmNlIGNsb3NlIGFsbCB0aGUgc3VibWVudXMgd2hlbiB0aGUgbWFpbiBtZW51IGNvbnRhaW5lciBpcyBjbG9zZWQuXG5cdGFwcC5mb3JjZUNsb3NlU3VibWVudXMgPSBmdW5jdGlvbigpIHtcblxuXHRcdC8vIFRoZSB0cmFuc2l0aW9uZW5kIGV2ZW50IHRyaWdnZXJzIG9uIG9wZW4gYW5kIG9uIGNsb3NlLCBuZWVkIHRvIG1ha2Ugc3VyZSB3ZSBvbmx5IGRvIHRoaXMgb24gY2xvc2UuXG5cdFx0aWYgKCAhICQoIHRoaXMgKS5oYXNDbGFzcyggJ2lzLXZpc2libGUnICkgKSB7XG5cdFx0XHRhcHAuJGMuc3ViTWVudVBhcmVudEl0ZW0ucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApLmZpbmQoICcucGFyZW50LWluZGljYXRvcicgKS5hdHRyKCAnYXJpYS1leHBhbmRlZCcsIGZhbHNlICk7XG5cdFx0XHRhcHAuJGMuc3ViTWVudUNvbnRhaW5lci5yZW1vdmVDbGFzcyggJ2lzLXZpc2libGUgc2xpZGVJbkxlZnQnICk7XG5cdFx0fVxuXG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xuXG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc01vYmlsZU1lbnUgKSApO1xuIiwiLyoqXG4gKiBGaWxlIG1vZGFsLmpzXG4gKlxuICogRGVhbCB3aXRoIG11bHRpcGxlIG1vZGFscyBhbmQgdGhlaXIgbWVkaWEuXG4gKi9cbndpbmRvdy53ZHNNb2RhbCA9IHt9O1xuKCBmdW5jdGlvbiggd2luZG93LCAkLCBhcHAgKSB7XG5cblx0bGV0ICRtb2RhbFRvZ2dsZSxcblx0XHQkZm9jdXNhYmxlQ2hpbGRyZW4sXG5cdFx0JHBsYXllcixcblx0XHQkdGFnID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCggJ3NjcmlwdCcgKSxcblx0XHQkZmlyc3RTY3JpcHRUYWcgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSggJ3NjcmlwdCcgKVswXSxcblx0XHRZVDtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHQkZmlyc3RTY3JpcHRUYWcucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoICR0YWcsICRmaXJzdFNjcmlwdFRhZyApO1xuXHRcdFx0YXBwLmJpbmRFdmVudHMoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gQ2FjaGUgYWxsIHRoZSB0aGluZ3MuXG5cdGFwcC5jYWNoZSA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYyA9IHtcblx0XHRcdCdib2R5JzogJCggJ2JvZHknIClcblx0XHR9O1xuXHR9O1xuXG5cdC8vIERvIHdlIG1lZXQgdGhlIHJlcXVpcmVtZW50cz9cblx0YXBwLm1lZXRzUmVxdWlyZW1lbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuICQoICcubW9kYWwtdHJpZ2dlcicgKS5sZW5ndGg7XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0Ly8gVHJpZ2dlciBhIG1vZGFsIHRvIG9wZW4uXG5cdFx0YXBwLiRjLmJvZHkub24oICdjbGljayB0b3VjaHN0YXJ0JywgJy5tb2RhbC10cmlnZ2VyJywgYXBwLm9wZW5Nb2RhbCApO1xuXG5cdFx0Ly8gVHJpZ2dlciB0aGUgY2xvc2UgYnV0dG9uIHRvIGNsb3NlIHRoZSBtb2RhbC5cblx0XHRhcHAuJGMuYm9keS5vbiggJ2NsaWNrIHRvdWNoc3RhcnQnLCAnLmNsb3NlJywgYXBwLmNsb3NlTW9kYWwgKTtcblxuXHRcdC8vIEFsbG93IHRoZSB1c2VyIHRvIGNsb3NlIHRoZSBtb2RhbCBieSBoaXR0aW5nIHRoZSBlc2Mga2V5LlxuXHRcdGFwcC4kYy5ib2R5Lm9uKCAna2V5ZG93bicsIGFwcC5lc2NLZXlDbG9zZSApO1xuXG5cdFx0Ly8gQWxsb3cgdGhlIHVzZXIgdG8gY2xvc2UgdGhlIG1vZGFsIGJ5IGNsaWNraW5nIG91dHNpZGUgb2YgdGhlIG1vZGFsLlxuXHRcdGFwcC4kYy5ib2R5Lm9uKCAnY2xpY2sgdG91Y2hzdGFydCcsICdkaXYubW9kYWwtb3BlbicsIGFwcC5jbG9zZU1vZGFsQnlDbGljayApO1xuXG5cdFx0Ly8gTGlzdGVuIHRvIHRhYnMsIHRyYXAga2V5Ym9hcmQgaWYgd2UgbmVlZCB0b1xuXHRcdGFwcC4kYy5ib2R5Lm9uKCAna2V5ZG93bicsIGFwcC50cmFwS2V5Ym9hcmRNYXliZSApO1xuXG5cdH07XG5cblx0Ly8gT3BlbiB0aGUgbW9kYWwuXG5cdGFwcC5vcGVuTW9kYWwgPSBmdW5jdGlvbigpIHtcblxuXHRcdC8vIFN0b3JlIHRoZSBtb2RhbCB0b2dnbGUgZWxlbWVudFxuXHRcdCRtb2RhbFRvZ2dsZSA9ICQoIHRoaXMgKTtcblxuXHRcdC8vIEZpZ3VyZSBvdXQgd2hpY2ggbW9kYWwgd2UncmUgb3BlbmluZyBhbmQgc3RvcmUgdGhlIG9iamVjdC5cblx0XHRsZXQgJG1vZGFsID0gJCggJCggdGhpcyApLmRhdGEoICd0YXJnZXQnICkgKTtcblxuXHRcdC8vIERpc3BsYXkgdGhlIG1vZGFsLlxuXHRcdCRtb2RhbC5hZGRDbGFzcyggJ21vZGFsLW9wZW4nICk7XG5cblx0XHQvLyBBZGQgYm9keSBjbGFzcy5cblx0XHRhcHAuJGMuYm9keS5hZGRDbGFzcyggJ21vZGFsLW9wZW4nICk7XG5cblx0XHQvLyBGaW5kIHRoZSBmb2N1c2FibGUgY2hpbGRyZW4gb2YgdGhlIG1vZGFsLlxuXHRcdC8vIFRoaXMgbGlzdCBtYXkgYmUgaW5jb21wbGV0ZSwgcmVhbGx5IHdpc2ggalF1ZXJ5IGhhZCB0aGUgOmZvY3VzYWJsZSBwc2V1ZG8gbGlrZSBqUXVlcnkgVUkgZG9lcy5cblx0XHQvLyBGb3IgbW9yZSBhYm91dCA6aW5wdXQgc2VlOiBodHRwczovL2FwaS5qcXVlcnkuY29tL2lucHV0LXNlbGVjdG9yL1xuXHRcdCRmb2N1c2FibGVDaGlsZHJlbiA9ICRtb2RhbC5maW5kKCAnYSwgOmlucHV0LCBbdGFiaW5kZXhdJyApO1xuXG5cdFx0Ly8gSWRlYWxseSwgdGhlcmUgaXMgYWx3YXlzIG9uZSAodGhlIGNsb3NlIGJ1dHRvbiksIGJ1dCB5b3UgbmV2ZXIga25vdy5cblx0XHRpZiAoIDAgPCAkZm9jdXNhYmxlQ2hpbGRyZW4ubGVuZ3RoICkge1xuXG5cdFx0XHQvLyBTaGlmdCBmb2N1cyB0byB0aGUgZmlyc3QgZm9jdXNhYmxlIGVsZW1lbnQuXG5cdFx0XHQkZm9jdXNhYmxlQ2hpbGRyZW5bMF0uZm9jdXMoKTtcblx0XHR9XG5cblx0fTtcblxuXHQvLyBDbG9zZSB0aGUgbW9kYWwuXG5cdGFwcC5jbG9zZU1vZGFsID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBGaWd1cmUgdGhlIG9wZW5lZCBtb2RhbCB3ZSdyZSBjbG9zaW5nIGFuZCBzdG9yZSB0aGUgb2JqZWN0LlxuXHRcdGxldCAkbW9kYWwgPSAkKCAkKCAnZGl2Lm1vZGFsLW9wZW4gLmNsb3NlJyApLmRhdGEoICd0YXJnZXQnICkgKSxcblxuXHRcdFx0Ly8gRmluZCB0aGUgaWZyYW1lIGluIHRoZSAkbW9kYWwgb2JqZWN0LlxuXHRcdFx0JGlmcmFtZSA9ICRtb2RhbC5maW5kKCAnaWZyYW1lJyApO1xuXG5cdFx0Ly8gT25seSBkbyB0aGlzIGlmIHRoZXJlIGFyZSBhbnkgaWZyYW1lcy5cblx0XHRpZiAoICRpZnJhbWUubGVuZ3RoICkge1xuXG5cdFx0XHQvLyBHZXQgdGhlIGlmcmFtZSBzcmMgVVJMLlxuXHRcdFx0bGV0IHVybCA9ICRpZnJhbWUuYXR0ciggJ3NyYycgKTtcblxuXHRcdFx0Ly8gUmVtb3ZpbmcvUmVhZGRpbmcgdGhlIFVSTCB3aWxsIGVmZmVjdGl2ZWx5IGJyZWFrIHRoZSBZb3VUdWJlIEFQSS5cblx0XHRcdC8vIFNvIGxldCdzIG5vdCBkbyB0aGF0IHdoZW4gdGhlIGlmcmFtZSBVUkwgY29udGFpbnMgdGhlIGVuYWJsZWpzYXBpIHBhcmFtZXRlci5cblx0XHRcdGlmICggISB1cmwuaW5jbHVkZXMoICdlbmFibGVqc2FwaT0xJyApICkge1xuXG5cdFx0XHRcdC8vIFJlbW92ZSB0aGUgc291cmNlIFVSTCwgdGhlbiBhZGQgaXQgYmFjaywgc28gdGhlIHZpZGVvIGNhbiBiZSBwbGF5ZWQgYWdhaW4gbGF0ZXIuXG5cdFx0XHRcdCRpZnJhbWUuYXR0ciggJ3NyYycsICcnICkuYXR0ciggJ3NyYycsIHVybCApO1xuXHRcdFx0fSBlbHNlIHtcblxuXHRcdFx0XHQvLyBVc2UgdGhlIFlvdVR1YmUgQVBJIHRvIHN0b3AgdGhlIHZpZGVvLlxuXHRcdFx0XHQkcGxheWVyLnN0b3BWaWRlbygpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdC8vIEZpbmFsbHksIGhpZGUgdGhlIG1vZGFsLlxuXHRcdCRtb2RhbC5yZW1vdmVDbGFzcyggJ21vZGFsLW9wZW4nICk7XG5cblx0XHQvLyBSZW1vdmUgdGhlIGJvZHkgY2xhc3MuXG5cdFx0YXBwLiRjLmJvZHkucmVtb3ZlQ2xhc3MoICdtb2RhbC1vcGVuJyApO1xuXG5cdFx0Ly8gUmV2ZXJ0IGZvY3VzIGJhY2sgdG8gdG9nZ2xlIGVsZW1lbnRcblx0XHQkbW9kYWxUb2dnbGUuZm9jdXMoKTtcblxuXHR9O1xuXG5cdC8vIENsb3NlIGlmIFwiZXNjXCIga2V5IGlzIHByZXNzZWQuXG5cdGFwcC5lc2NLZXlDbG9zZSA9IGZ1bmN0aW9uKCBldmVudCApIHtcblx0XHRpZiAoIDI3ID09PSBldmVudC5rZXlDb2RlICkge1xuXHRcdFx0YXBwLmNsb3NlTW9kYWwoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gQ2xvc2UgaWYgdGhlIHVzZXIgY2xpY2tzIG91dHNpZGUgb2YgdGhlIG1vZGFsXG5cdGFwcC5jbG9zZU1vZGFsQnlDbGljayA9IGZ1bmN0aW9uKCBldmVudCApIHtcblxuXHRcdC8vIElmIHRoZSBwYXJlbnQgY29udGFpbmVyIGlzIE5PVCB0aGUgbW9kYWwgZGlhbG9nIGNvbnRhaW5lciwgY2xvc2UgdGhlIG1vZGFsXG5cdFx0aWYgKCAhICQoIGV2ZW50LnRhcmdldCApLnBhcmVudHMoICdkaXYnICkuaGFzQ2xhc3MoICdtb2RhbC1kaWFsb2cnICkgKSB7XG5cdFx0XHRhcHAuY2xvc2VNb2RhbCgpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBUcmFwIHRoZSBrZXlib2FyZCBpbnRvIGEgbW9kYWwgd2hlbiBvbmUgaXMgYWN0aXZlLlxuXHRhcHAudHJhcEtleWJvYXJkTWF5YmUgPSBmdW5jdGlvbiggZXZlbnQgKSB7XG5cblx0XHQvLyBXZSBvbmx5IG5lZWQgdG8gZG8gc3R1ZmYgd2hlbiB0aGUgbW9kYWwgaXMgb3BlbiBhbmQgdGFiIGlzIHByZXNzZWQuXG5cdFx0aWYgKCA5ID09PSBldmVudC53aGljaCAmJiAwIDwgJCggJy5tb2RhbC1vcGVuJyApLmxlbmd0aCApIHtcblx0XHRcdGxldCAkZm9jdXNlZCA9ICQoICc6Zm9jdXMnICksXG5cdFx0XHRcdGZvY3VzSW5kZXggPSAkZm9jdXNhYmxlQ2hpbGRyZW4uaW5kZXgoICRmb2N1c2VkICk7XG5cblx0XHRcdGlmICggMCA9PT0gZm9jdXNJbmRleCAmJiBldmVudC5zaGlmdEtleSApIHtcblxuXHRcdFx0XHQvLyBJZiB0aGlzIGlzIHRoZSBmaXJzdCBmb2N1c2FibGUgZWxlbWVudCwgYW5kIHNoaWZ0IGlzIGhlbGQgd2hlbiBwcmVzc2luZyB0YWIsIGdvIGJhY2sgdG8gbGFzdCBmb2N1c2FibGUgZWxlbWVudC5cblx0XHRcdFx0JGZvY3VzYWJsZUNoaWxkcmVuWyAkZm9jdXNhYmxlQ2hpbGRyZW4ubGVuZ3RoIC0gMSBdLmZvY3VzKCk7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHR9IGVsc2UgaWYgKCAhIGV2ZW50LnNoaWZ0S2V5ICYmIGZvY3VzSW5kZXggPT09ICRmb2N1c2FibGVDaGlsZHJlbi5sZW5ndGggLSAxICkge1xuXG5cdFx0XHRcdC8vIElmIHRoaXMgaXMgdGhlIGxhc3QgZm9jdXNhYmxlIGVsZW1lbnQsIGFuZCBzaGlmdCBpcyBub3QgaGVsZCwgZ28gYmFjayB0byB0aGUgZmlyc3QgZm9jdXNhYmxlIGVsZW1lbnQuXG5cdFx0XHRcdCRmb2N1c2FibGVDaGlsZHJlblswXS5mb2N1cygpO1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0fVxuXHRcdH1cblx0fTtcblxuXHQvLyBIb29rIGludG8gWW91VHViZSA8aWZyYW1lPi5cblx0YXBwLm9uWW91VHViZUlmcmFtZUFQSVJlYWR5ID0gZnVuY3Rpb24oKSB7XG5cdFx0bGV0ICRtb2RhbCA9ICQoICdkaXYubW9kYWwnICksXG5cdFx0XHQkaWZyYW1laWQgPSAkbW9kYWwuZmluZCggJ2lmcmFtZScgKS5hdHRyKCAnaWQnICk7XG5cblx0XHQkcGxheWVyID0gbmV3IFlULlBsYXllciggJGlmcmFtZWlkLCB7XG5cdFx0XHRldmVudHM6IHtcblx0XHRcdFx0J29uUmVhZHknOiBhcHAub25QbGF5ZXJSZWFkeSxcblx0XHRcdFx0J29uU3RhdGVDaGFuZ2UnOiBhcHAub25QbGF5ZXJTdGF0ZUNoYW5nZVxuXHRcdFx0fVxuXHRcdH0pO1xuXHR9O1xuXG5cdC8vIERvIHNvbWV0aGluZyBvbiBwbGF5ZXIgcmVhZHkuXG5cdGFwcC5vblBsYXllclJlYWR5ID0gZnVuY3Rpb24oKSB7XG5cdH07XG5cblx0Ly8gRG8gc29tZXRoaW5nIG9uIHBsYXllciBzdGF0ZSBjaGFuZ2UuXG5cdGFwcC5vblBsYXllclN0YXRlQ2hhbmdlID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBTZXQgZm9jdXMgdG8gdGhlIGZpcnN0IGZvY3VzYWJsZSBlbGVtZW50IGluc2lkZSBvZiB0aGUgbW9kYWwgdGhlIHBsYXllciBpcyBpbi5cblx0XHQkKCBldmVudC50YXJnZXQuYSApLnBhcmVudHMoICcubW9kYWwnICkuZmluZCggJ2EsIDppbnB1dCwgW3RhYmluZGV4XScgKS5maXJzdCgpLmZvY3VzKCk7XG5cdH07XG5cblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc01vZGFsICkgKTtcbiIsIi8qKlxuICogRmlsZTogbmF2aWdhdGlvbi1wcmltYXJ5LmpzXG4gKlxuICogSGVscGVycyBmb3IgdGhlIHByaW1hcnkgbmF2aWdhdGlvbi5cbiAqL1xud2luZG93Lndkc1ByaW1hcnlOYXZpZ2F0aW9uID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKCB3aW5kb3cgKSxcblx0XHRcdHN1Yk1lbnVDb250YWluZXI6ICQoICcubWFpbi1uYXZpZ2F0aW9uIC5zdWItbWVudScgKSxcblx0XHRcdHN1Yk1lbnVQYXJlbnRJdGVtOiAkKCAnLm1haW4tbmF2aWdhdGlvbiBsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApXG5cdFx0fTtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLndpbmRvdy5vbiggJ2xvYWQnLCBhcHAuYWRkRG93bkFycm93ICk7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLmZpbmQoICdhJyApLm9uKCAnZm9jdXNpbiBmb2N1c291dCcsIGFwcC50b2dnbGVGb2N1cyApO1xuXHR9O1xuXG5cdC8vIERvIHdlIG1lZXQgdGhlIHJlcXVpcmVtZW50cz9cblx0YXBwLm1lZXRzUmVxdWlyZW1lbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIGFwcC4kYy5zdWJNZW51Q29udGFpbmVyLmxlbmd0aDtcblx0fTtcblxuXHQvLyBBZGQgdGhlIGRvd24gYXJyb3cgdG8gc3VibWVudSBwYXJlbnRzLlxuXHRhcHAuYWRkRG93bkFycm93ID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLmZpbmQoICc+IGEnICkuYXBwZW5kKCAnPHNwYW4gY2xhc3M9XCJjYXJldC1kb3duXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9zcGFuPicgKTtcblx0fTtcblxuXHQvLyBUb2dnbGUgdGhlIGZvY3VzIGNsYXNzIG9uIHRoZSBsaW5rIHBhcmVudC5cblx0YXBwLnRvZ2dsZUZvY3VzID0gZnVuY3Rpb24oKSB7XG5cdFx0JCggdGhpcyApLnBhcmVudHMoICdsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApLnRvZ2dsZUNsYXNzKCAnZm9jdXMnICk7XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xuXG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc1ByaW1hcnlOYXZpZ2F0aW9uICkgKTtcbiIsIi8qKlxuICogRmlsZTogb2ZmLWNhbnZhcy5qc1xuICpcbiAqIEhlbHAgZGVhbCB3aXRoIHRoZSBvZmYtY2FudmFzIG1vYmlsZSBtZW51LlxuICovXG53aW5kb3cud2Rzb2ZmQ2FudmFzID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0Ym9keTogJCggJ2JvZHknICksXG5cdFx0XHRvZmZDYW52YXNDbG9zZTogJCggJy5vZmYtY2FudmFzLWNsb3NlJyApLFxuXHRcdFx0b2ZmQ2FudmFzQ29udGFpbmVyOiAkKCAnLm9mZi1jYW52YXMtY29udGFpbmVyJyApLFxuXHRcdFx0b2ZmQ2FudmFzT3BlbjogJCggJy5vZmYtY2FudmFzLW9wZW4nICksXG5cdFx0XHRvZmZDYW52YXNTY3JlZW46ICQoICcub2ZmLWNhbnZhcy1zY3JlZW4nIClcblx0XHR9O1xuXHR9O1xuXG5cdC8vIENvbWJpbmUgYWxsIGV2ZW50cy5cblx0YXBwLmJpbmRFdmVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMuYm9keS5vbiggJ2tleWRvd24nLCBhcHAuZXNjS2V5Q2xvc2UgKTtcblx0XHRhcHAuJGMub2ZmQ2FudmFzQ2xvc2Uub24oICdjbGljaycsIGFwcC5jbG9zZW9mZkNhbnZhcyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNPcGVuLm9uKCAnY2xpY2snLCBhcHAudG9nZ2xlb2ZmQ2FudmFzICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc1NjcmVlbi5vbiggJ2NsaWNrJywgYXBwLmNsb3Nlb2ZmQ2FudmFzICk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gYXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5sZW5ndGg7XG5cdH07XG5cblx0Ly8gVG8gc2hvdyBvciBub3QgdG8gc2hvdz9cblx0YXBwLnRvZ2dsZW9mZkNhbnZhcyA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0aWYgKCAndHJ1ZScgPT09ICQoIHRoaXMgKS5hdHRyKCAnYXJpYS1leHBhbmRlZCcgKSApIHtcblx0XHRcdGFwcC5jbG9zZW9mZkNhbnZhcygpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRhcHAub3Blbm9mZkNhbnZhcygpO1xuXHRcdH1cblxuXHR9O1xuXG5cdC8vIFNob3cgdGhhdCBkcmF3ZXIhXG5cdGFwcC5vcGVub2ZmQ2FudmFzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5hZGRDbGFzcyggJ2lzLXZpc2libGUnICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYWRkQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNTY3JlZW4uYWRkQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCB0cnVlICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5hdHRyKCAnYXJpYS1oaWRkZW4nLCBmYWxzZSApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5maW5kKCAnYnV0dG9uJyApLmZpcnN0KCkuZm9jdXMoKTtcblx0fTtcblxuXHQvLyBDbG9zZSB0aGF0IGRyYXdlciFcblx0YXBwLmNsb3Nlb2ZmQ2FudmFzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5yZW1vdmVDbGFzcyggJ2lzLXZpc2libGUnICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4ucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNTY3JlZW4ucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCBmYWxzZSApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNDb250YWluZXIuYXR0ciggJ2FyaWEtaGlkZGVuJywgdHJ1ZSApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uZm9jdXMoKTtcblx0fTtcblxuXHQvLyBDbG9zZSBkcmF3ZXIgaWYgXCJlc2NcIiBrZXkgaXMgcHJlc3NlZC5cblx0YXBwLmVzY0tleUNsb3NlID0gZnVuY3Rpb24oIGV2ZW50ICkge1xuXHRcdGlmICggMjcgPT09IGV2ZW50LmtleUNvZGUgKSB7XG5cdFx0XHRhcHAuY2xvc2VvZmZDYW52YXMoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xuXG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc29mZkNhbnZhcyApICk7XG4iLCIvKipcbiAqIEZpbGUgc2tpcC1saW5rLWZvY3VzLWZpeC5qcy5cbiAqXG4gKiBIZWxwcyB3aXRoIGFjY2Vzc2liaWxpdHkgZm9yIGtleWJvYXJkIG9ubHkgdXNlcnMuXG4gKlxuICogTGVhcm4gbW9yZTogaHR0cHM6Ly9naXQuaW8vdldkcjJcbiAqL1xuKCBmdW5jdGlvbigpIHtcblx0dmFyIGlzV2Via2l0ID0gLTEgPCBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZiggJ3dlYmtpdCcgKSxcblx0XHRpc09wZXJhID0gLTEgPCBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZiggJ29wZXJhJyApLFxuXHRcdGlzSWUgPSAtMSA8IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5pbmRleE9mKCAnbXNpZScgKTtcblxuXHRpZiAoICggaXNXZWJraXQgfHwgaXNPcGVyYSB8fCBpc0llICkgJiYgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgKSB7XG5cdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdoYXNoY2hhbmdlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgaWQgPSBsb2NhdGlvbi5oYXNoLnN1YnN0cmluZyggMSApLFxuXHRcdFx0XHRlbGVtZW50O1xuXG5cdFx0XHRpZiAoICEgKCAvXltBLXowLTlfLV0rJC8gKS50ZXN0KCBpZCApICkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggaWQgKTtcblxuXHRcdFx0aWYgKCBlbGVtZW50ICkge1xuXHRcdFx0XHRpZiAoICEgKCAvXig/OmF8c2VsZWN0fGlucHV0fGJ1dHRvbnx0ZXh0YXJlYSkkL2kgKS50ZXN0KCBlbGVtZW50LnRhZ05hbWUgKSApIHtcblx0XHRcdFx0XHRlbGVtZW50LnRhYkluZGV4ID0gLTE7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRlbGVtZW50LmZvY3VzKCk7XG5cdFx0XHR9XG5cdFx0fSwgZmFsc2UgKTtcblx0fVxufSgpICk7XG4iLCIvKipcbiAqIEZpbGUgd2luZG93LXJlYWR5LmpzXG4gKlxuICogQWRkIGEgXCJyZWFkeVwiIGNsYXNzIHRvIDxib2R5PiB3aGVuIHdpbmRvdyBpcyByZWFkeS5cbiAqL1xud2luZG93Lndkc1dpbmRvd1JlYWR5ID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHR9O1xuXG5cdC8vIENhY2hlIGRvY3VtZW50IGVsZW1lbnRzLlxuXHRhcHAuY2FjaGUgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMgPSB7XG5cdFx0XHQnd2luZG93JzogJCggd2luZG93ICksXG5cdFx0XHQnYm9keSc6ICQoIGRvY3VtZW50LmJvZHkgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy53aW5kb3cubG9hZCggYXBwLmFkZEJvZHlDbGFzcyApO1xuXHR9O1xuXG5cdC8vIEFkZCBhIGNsYXNzIHRvIDxib2R5Pi5cblx0YXBwLmFkZEJvZHlDbGFzcyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy5ib2R5LmFkZENsYXNzKCAncmVhZHknICk7XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xufSggd2luZG93LCBqUXVlcnksIHdpbmRvdy53ZHNXaW5kb3dSZWFkeSApICk7XG4iXX0=
